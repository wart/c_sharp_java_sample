﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RNG.helpers;
using System.Threading;
namespace RNG
{
    public class Manager
    {
        // ---------------------------------------
        private class Model
        {
            private decimal  id;
            private bool     IsCorp;
            private bool     IsCard;
            private string   name;
            private string   cap;
            private List<Report> src;
            private Model(decimal _id, bool _IsCorp, bool _IsCard, string _name, string _cap)
            {
                id=_id;
                IsCorp = _IsCorp;
                IsCard = _IsCard;
                name = _name;
                cap = _cap;
                src = null;
            }
            // -----------------------------------
            internal static Model get(string name)
            {
                var s=Select.Exec(SQL.ModelDesc, name);
                if (s != null)
                {
                    Model mdl = null;
                    if (s.odr.Read())
                        mdl = new Model((int)s.odr.GetDecimal(0),
                                         (s.odr.GetString(1)[0] == 'Y') ? true : false,
                                         (s.odr.GetString(2)[0] == 'Y') ? true : false,
                                         name,
                                         s.odr.GetString(3));
                    s.Release();
                    return mdl;
                }
                return null;
            }
            // -----------------------------------
            internal Report GetReport(DateTime actDT)
            {
                if (src == null)
                {
                    src = new List<Report>();
                    var s = Select.Exec(SQL.ModelSources,id);
                    if (s != null)
                    {
                        while (s.odr.Read())
                        {
                            //bool _IsCorp, bool _IsCard, string _name, string _cap, decimal _gid, decimal _rid, decimal _pid, 
                            //  decimal _gen,string _ctype, string _fext, DateTime _beg, DateTime _end
                            src.Add(new Report( IsCorp,IsCard,name,cap,
                                                (int)s.odr.GetDecimal(0),  //gid
                                                id,
                                                (int)s.odr.GetDecimal(1),  //pid
                                                toENUM.GT((int)s.odr.GetDecimal(2)),  //gen
                                                     s.odr.GetString(3),    //ctype   
                                                     s.odr.GetString(4),    // ext
                                                     s.odr.GetDateTime(5),  //beg
                                                     s.odr.IsDBNull(6) ? DateTime.Now.AddYears(666) : s.odr.GetDateTime(6), //end
                                                     s.odr.GetString(7), s.odr.IsDBNull(8) ?"":s.odr.GetString(8)
                                              ));//end
                        };
                        s.Release();
                    }
                }
                if (src != null)
                  try
                    {
                        return src.First(s => ((s.beg <= actDT) && (s.end >= actDT)));
                    }catch(Exception e){};

                return null;
            }
            // -----------------------------------
        }
        // ---------------------------------------
        private Dictionary<string, Model> cache_model;
        // ---------------------------------------
        private Model getModel(string name)
        {
            if(cache_model==null) cache_model=new Dictionary<string,Model>();
            if (!cache_model.ContainsKey(name))
            {
                Model m = Model.get(name);
                if (m != null)
                {
                    cache_model.Add(name, m);
                    return m;
                }
            }
            else
                return cache_model[name];
            return null;
        }
        // ---------------------------------------
        public Report getReport(string name, DateTime act)
        {
            Model m = getModel(name);
            if(m!=null)
                return m.GetReport(act);
            return null;
        }
        // ---------------------------------------
        private Dictionary<int, ParameterList> cache_paramlist;
        // ---------------------------------------
        private ParameterList getParameterList(int list_id)
        {
            if (cache_paramlist == null) cache_paramlist = new Dictionary<int, ParameterList>();
            if (!cache_paramlist.ContainsKey(list_id))
            {
                var s=Select.Exec(SQL.ParamListDesc, list_id);
                if (s != null)
                {
                    ParameterList pl = null;
                    if (s.odr.Read())
                    {
                        pl = new ParameterList(list_id, s.odr.GetString(0));
                        cache_paramlist.Add(list_id, pl);
                    }
                    s.Release();
                    return pl;
                }
            }
            else
                return cache_paramlist[list_id];
            return null;
        }
        // ---------------------------------------
        private Dictionary<int, ParameterDesc> cache_paramdesc;
        // ---------------------------------------
        private ParameterDesc getParameterDesc(int desc_id)
        {
            if (cache_paramdesc == null) cache_paramdesc = new Dictionary<int, ParameterDesc>();
            if (!cache_paramdesc.ContainsKey(desc_id))
            {
                var s = Select.Exec(SQL.ParamDesc, desc_id);
                if (s != null)
                {
                    ParameterDesc pd = null;
                    if (s.odr.Read())
                    {
                        pd = new ParameterDesc( s.odr.GetString(0), 
                                                toENUM.PT((int)s.odr.GetDecimal(1)),
                                                s.odr.GetString(2),
                                                s.odr.IsDBNull(3) ? "" : s.odr.GetString(3),
                                                s.odr.IsDBNull(4) ? "" : s.odr.GetString(4),
                                                s.odr.IsDBNull(5) ? "" : s.odr.GetString(5));
                        cache_paramdesc.Add(desc_id, pd);
                    }
                    s.Release();
                    return pd;
                }
            }
            else
                return cache_paramdesc[desc_id];
            return null;
        }
        // ---------------------------------------
        private List<ParameterDesc> getListParameterDesc(int list_id)
        {
            List<ParameterDesc> ret = new List<ParameterDesc>();
            var s = Select.Exec(SQL.ParamListItems, list_id);
            if (s != null)
            {
                while (s.odr.Read())
                {
                    ParameterDesc pd = getParameterDesc((int)s.odr.GetDecimal(0));
                    if (pd != null)
                        ret.Add(pd);
                }
                s.Release();
            }
            return ret;
        }
        // ---------------------------------------
        public List<ParameterDesc> getParams(Report r)
        {
            if (r != null)
            {
                var pl=getParameterList((int)r.pid);
                if (pl != null)
                {
                    if (pl.Params == null)
                    {
                        pl.Params = getListParameterDesc(pl.id);
                    }
                    return pl.Params;
                }
            }
            return null;
        }
        // ---------------------------------------
        public List<ReportDesc> getReportList(int id)
        {
            List<ReportDesc> ret = null;
            Select s=null;
            if (id == 0)
            {
                s = Select.Exec(SQL.ReportList);
            }else
                s = Select.Exec(SQL.ReportListId,id);
            if (s != null)
            {
                ret = new List<ReportDesc>();
                while (s.odr.Read())
                {
                    ReportDesc pd = new ReportDesc(s.odr.GetString(0), s.odr.GetString(1));
                    if (pd != null)
                        ret.Add(pd);
                }
                s.Release();
            }
            return ret;
        }
        // ---------------------------------------
        public static void test()
        {
            Manager m= new Manager();
           
        }
    }
    // ---------------------------------------
    public class Report
    {
        internal bool IsCorp;
        internal bool IsCard;
        internal string  name;
        internal string  cap;
        internal string  format;
        internal string  arg;
        //--------------------------
        internal decimal gid;
        internal decimal rid;
        internal decimal pid;
        internal GeneratorType gen;
        internal string  ctype;
        internal string  fext;
        internal DateTime beg;
        internal DateTime end;
        //--------------------------
        internal bool    src_init ;
        internal object  body;
        //--------------------------
        internal Report(bool _IsCorp, bool _IsCard, string _name, string _cap, decimal _gid, decimal _rid, decimal _pid, GeneratorType _gen, string _ctype, string _fext, DateTime _beg, DateTime _end, string _format, string _arg)
        {
            IsCorp=_IsCorp;     IsCard = _IsCard;      name = _name;       cap=_cap;
            gid = _gid;         rid = _rid;            pid = _pid;         ctype = _ctype;
            fext = _fext;       beg = _beg;            end = _end;          src_init = false;
            gen = _gen; format = _format; arg = _arg;
        }
        //--------------------------
        public Result Build(Dictionary<string, object> Args)
        {
            if (Args == null) Args = new Dictionary<string, object>();
            switch(gen)
            {
                case GeneratorType.RDF: return RDF.Build(this, Args, arg);
                case GeneratorType.TML: return TML.Build(this, Args);
            };
            return null;
        }
        //--------------------------
    }
    // ---------------------------------------
    public class Result
    {
        internal Report rep;
        internal Dictionary<string, object> args;
        internal Thread oThread;
        internal System.IO.Stream sw;
        // -----------------------------------
        internal Result(Report _rep, Dictionary<string, object> _args)
        {
            args= rep4ead.tool.CopyDictionary(_args,null,"");
            rep = _rep;
            oThread=null;
            sw=null;
        }
        // -----------------------------------
        public System.IO.Stream getData()
        {
            oThread.Join();
            return sw;        
        }
        // -----------------------------------
        public string GetContentType()
        {
            return rep.ctype;
        }
        // -----------------------------------
        public string getFileDownloadName()
        {
            return rep.name+"."+rep.fext;
        }
        // -----------------------------------
    }
    // -------------------------------------------------------
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rep4ead
{
    class Parcer
    {
	    private	int cor_pos;
	    private	int mode;
	    private	int rettype;
	    private Stack<Node> stack;
        private Node rootNode;
	    public const String tag_Begin   ="<%";
	    public const char tag_inSplit =';';
	    public const String tag_End	   ="%>";
	
//	    public  static bool logging=true; 

	    //=====================================================
	    public Parcer()
	    {
		    start_parce();
	    }
	    //=====================================================
	    private void start_parce()
	    {
		    cor_pos=-1;
		    mode=0;
		    rettype=0;
            stack = new Stack<Node>();
            rootNode=Node.NodeRoot();
            stack.Push(rootNode);
	    }
	    //=====================================================
	    private bool pushContaneer(Node n)
	    {
		    if(n!=null)
			    if(pushChild(n))
			    {
                    stack.Push(n);
				    return true;
			    };
		    return false;
	    }
	    //=====================================================
	    private bool pushChild(Node n)
	    {
		    if((n!=null)&&(stack.Count>0))
		    {
			    Node p=stack.Peek();
			    if(p!=null)
			    {
				    p.AddNode(n);
				    return true;
			    };
		    };
		    return false;
	    }
	    //=====================================================
	    private bool stepUp()
	    {
            if (stack.Count > 1)
		    {
                stack.Pop();
			    return true;
		    }else
			    return false;
	    }
	    //=====================================================
	    private bool stepSwitch(int md)
	    {
			if((stack!=null)&&(stack.Count>0))
			{
                Node n = stack.Peek();
				if(n!=null)
					return n.allowAndSwitchCh2(md);
			};
		    return false;
	    }
	    //=====================================================
	    private String get(String src)
	    {
		    if(src==null) return null;
		    int len=src.Length;
		    if(len<=cor_pos) return null;
		    bool repeat=true;
		    while(repeat)
		    {
					
			    repeat=false;
			    switch(mode)
			    {
				    case 0:
				    {
					    int i=(cor_pos<0)?src.IndexOf(tag_Begin):src.IndexOf(tag_Begin, cor_pos);
					    cor_pos=(cor_pos<0)?0:cor_pos;
					    if(i>=0)
					    {
						    if (cor_pos!=i)
						    {
							    String ret=src.Substring(cor_pos,i-cor_pos);
							    cor_pos=i+tag_Begin.Length;
							    mode=1;
							    rettype=0;
							    return ret;
						    }else
						    {
							    cor_pos=i+tag_Begin.Length;
							    mode=1;
							    rettype=0;
							    repeat=true;
						    }
					    }else
					    {
						    String ret=src.Substring(cor_pos);
						    cor_pos=len;
						    rettype=0;
						    return ret;
					    }
				    };break;
				    case 1:
				    {
					    int i=(cor_pos<0)?src.IndexOf(tag_End):src.IndexOf(tag_End, cor_pos);
					    cor_pos=(cor_pos<0)?0:cor_pos;
					    if(i>=0)
					    {
						    if (cor_pos!=i)
						    {
							    String ret=src.Substring(cor_pos,i-cor_pos);
							    cor_pos=i+tag_End.Length;
							    mode=0;
							    rettype=1;
							    return ret;
						    }else
						    {
							    cor_pos=i+tag_End.Length;
							    mode=0;rettype=1;
							    repeat=true;
						    }
					    }else
					    {
						    cor_pos=len;
						    rettype=2;
						    return null;
					    }
				    };break;
			    };
		    };
		    return null;
 	    }
	    //=====================================================
	    private void debug_strings(String[] s)
	    {
		    if(s!=null)
		    {
                for(int i =0;i<s.Length;++i)
                    Console.WriteLine("{0}: {1}",i,s[i]);
            }
            else Console.WriteLine("String[] s=null");
	    }
	    //=====================================================
	    private bool pushNode(String t)
	    {
		    if (t!=null)
		    {
				    String[] s=t.Split(tag_inSplit);//.ToCharArray(),tag_inSplit.Length);
                    for (int i = 0; i < s.Length; ++i)
                        s[i] = s[i].Trim();

                    s[0] = s[0].ToUpper();
		    //		debug_strings(s);
				    if(s!=null)
				    {
                             if (s[0].Equals(Node.tag_SINGLE    )) {return pushChild(Node.initp3  (Node.ID_NODE_SINGLE,  s));}
                        else if (s[0].Equals(Node.tag_SINGLE_AS )) {return pushChild(Node.initp3  (Node.ID_NODE_SINGLEAS, s));}
                        else if (s[0].Equals(Node.tag_STRF      )) {return pushChild(Node.initp3  (Node.ID_NODE_STRF,    s));}
                        else if (s[0].Equals(Node.tag_STRFAS    )) {return pushChild(Node.initp3  (Node.ID_NODE_STRFAS,  s));}
                        else if (s[0].Equals(Node.tag_SSET      )) {return pushChild(Node.initp3  (Node.ID_NODE_SSET, s, 0)); }
                        else if (s[0].Equals(Node.tag_EXEC      )) {return pushChild(Node.initp2v (Node.ID_NODE_EXEC, s)); }
                        else if (s[0].Equals(Node.tag_PRN       )) {return pushChild(Node.initname(Node.ID_NODE_PRN, s)); }
                        else if (s[0].Equals(Node.tag_PUTBLOCK  )) {return pushChild(Node.initname(Node.ID_NODE_PUTBLOCK, s)); }
                  //    else if (s[0].Equals(Node.tag_REFC	    )) {return pushChild(Node.initp2v(Node.ID_NODE_REFC, s));}

                        else if (s[0].Equals(Node.tag_FETCH     )) {return pushContaneer(Node.initp3(Node.ID_NODE_FETCH, s)); }
                        else if (s[0].Equals(Node.tag_FETCH_AS  )) {return pushContaneer(Node.initp3(Node.ID_NODE_FETCHAS, s)); }

                        else if (s[0].Equals(Node.tag_ELSE      )) {return stepSwitch(Node.ID_NCHILDS_ELSE); }
                        else if (s[0].Equals(Node.tag_EMPTY	    )) {return stepSwitch(Node.ID_NCHILDS_EMPTY);}
                        else if (s[0].Equals(Node.tag_ZEBRA	    )) {return stepSwitch(Node.ID_NCHILDS_ZEBRA);}
                        else if (s[0].Equals(Node.tag_HEADER	)) {return stepSwitch(Node.ID_NCHILDS_HEADER);}
                        else if (s[0].Equals(Node.tag_BLOCK))
                        {
                            Node nn = Node.initp3(Node.ID_NODE_BLOCK, s, 2);
                            if (rootNode != null) rootNode.RootAddNamedBlock(nn);
						    return pushContaneer(nn);
                        }
                        else if(s[0].Equals(Node.tag_MULTI	)){
                                    string tt = t.Substring(t.IndexOf(tag_inSplit)+1).Replace("\r", "");
                                    String[] ss = tt.Split('\n');
                                    foreach(var cm in ss)
                                        if(cm!=null)
                                        {
                                            string cmd=cm.Trim();
                                            if(!cmd.Equals(""))
                                                if(!pushNode(cmd))
                                                    return false;
                                        }
                                    return true;
					
    				    }else if (s[0].Equals(Node.tag_END)) { return stepUp(); }
                         else return pushChild(Node.NodeStatic(tag_Begin+t+tag_End)); 
				    };
		    };
		    return false;
	    }
	    //=====================================================
        internal string getPosB(int pos,string src)
        {
            int l=1;int p=1;
	        for (int j=0;j<pos&&j<src.Length;j++)
            	if (src[j]=='\n')
		        {
			        l++;
			        p=1;
		        }else
			        p++;
	        return "Line: "+l+", pos: "+p;
        }
        //=====================================================
        public Node ParceTemplate(String src)// throws Exception
	    {
		       start_parce();
		       String o;
		       bool allOk=true;
		       while(((o = get(src))!=null)&&(allOk))
		       {
			   
			       switch (rettype)
			       {
				       case 0:allOk = pushChild(Node.NodeStatic(o));break;
				       case 1:allOk = pushNode(o);break;
                       case 2: allOk = false; break;
			       }
		       };
		       if(o!=null)   		   throw new Exception("Error parcing at "+getPosB(cor_pos,src));
		       if (stack.Count!=1)	   throw new Exception("Error tags closing ");

		       Node n=rootNode;
		       start_parce();
		       return n;		
	    }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rep4ead
{
    public class Builder
    {
        private class Iteration
        {
            public const int ID_CONTANEER = 0;
            public const int ID_FETCH = 1;
            //-----------------------------------------------------------------------
            public int Class;

            public List<Node> childs;
            public long step;

            public interfaces.ResultSet rs;
            public Node src;
            //-----------------------------------------------------------------------
            private Iteration(int cl) { Class = cl; }
            //-----------------------------------------------------------------------
            static public Iteration it_Fetch(Node n, interfaces.ResultSet r)
            {
                Iteration it = null;
                if ((n != null) && (r != null))
                {
                    it = new Iteration(ID_FETCH);
                    it.rs = r;
                    it.step = 0;
                    it.src = n;
                };
                return it;
            }
            //-----------------------------------------------------------------------
            static public Iteration it_Contaneer(List<Node> chl)
            {
                Iteration it = null;
                if (chl != null)
                    if (chl.Count > 0)
                    {
                        it = new Iteration(ID_CONTANEER);
                        it.childs = chl;
                        it.step = 0;
                    };
                return it;
            }
            //-----------------------------------------------------------------------
        }
        //-----------------------------------------------------------------------
        private Stack<Iteration>		stack;
	    private StringBuilder			sb;
	    private Dictionary<String,Object>	_params;
        private interfaces.Connection conn;
	    private Node					ROOT;
	    private bool					_debug;
	    //----------------------------------------------------------
        private Builder(Dictionary<String, Object> _in, StringBuilder sbb, interfaces.Connection con)
	    {
		    sb				=	(sbb==null)?new StringBuilder():sbb;
            stack = new Stack<Iteration>();
		    //params			=	new Dictionary<String,Object>();
		    conn			=	con;
		    _params=_in;
		    //tool.CopyTreeMap(in,params,"IN.");
	    }
	    //----------------------------------------------------------
	    private void pop()
	    {
		    if(stack.Count>0)stack.Pop();
	    }	
	    //----------------------------------------------------------
	    private void push(Iteration it)
	    {
		    if(stack.Count<1024)
		    {
			    if(it!=null)
			    stack.Push(it);
		    }else throw new Exception("Stack overflow");
	    }
	    //----------------------------------------------------------
	    private Node GetNamedBlock(String nam)
	    {
		    return (ROOT!=null)?ROOT.RootGetNamedBlock(nam):null;
	    }
	    //----------------------------------------------------------
        private interfaces.ResultSet init_request(String sql, List<String> n)
	    {
            var r = tool.init_request(sql, n, _params, conn, sb, _debug);
            if(r==null)
                sb.Append("Error init sql: " + sql);
		    return r;
	    }
        //----------------------------------------------------------
        private interfaces.ResultSet init_requestB(String prm, List<String> n)
        {
            if (_params.ContainsKey(prm))
                if(_params[prm].GetType().Name.Equals("String"))
                    return init_request((string)_params[prm], n);
            return null;
        }
        //----------------------------------------------------------
        private bool PrepareFields(interfaces.ResultSet rs, String pref)
	    {
		    //if()
		    return tool.PrepareFields(rs,pref+".",_params);
	    }
	    //----------------------------------------------------------
        private void SetVal(string prm,string val)
        {
            if (_params.ContainsKey(prm))
                _params[prm] = val; 
            else
                _params.Add(prm, val);
        }
        //----------------------------------------------------------
        private void prepare_Node(Node n)
	    {
		    if(n!=null)
		    {
			    switch(n.Class)
			    {
                    case Node.ID_NODE_ROOT: push(Iteration.it_Contaneer(n.getList(0))); break;
                    case    Node.ID_NODE_PRN     : if (_params.ContainsKey(n.name)) sb.Append(_params[n.name]);
                                                   break;
				    case	Node.ID_NODE_SINGLE  :PrepareFields(init_request(n.Value,n.param),n.name);break;
                    case    Node.ID_NODE_SINGLEAS:PrepareFields(init_requestB(n.Value, n.param), n.name); break;
				
				    case	Node.ID_NODE_EXEC	 :ExecEXEC(n);break;
				    case	Node.ID_NODE_REFC	 :ExecREFC(n);break;
				
				    case	Node.ID_NODE_FETCH	 :push(Iteration.it_Fetch(n,init_request (n.Value,n.param)));break;
                    case    Node.ID_NODE_FETCHAS :push(Iteration.it_Fetch(n,init_requestB(n.Value, n.param))); break;
				    case	Node.ID_NODE_STATIC	 :sb.Append(n.Value);break;
                    case Node.ID_NODE_BLOCK: if (n.allowBlockBuildInPlace()) push(Iteration.it_Contaneer(n.getList(0))); break;
                    case Node.ID_NODE_PUTBLOCK: { Node nn = GetNamedBlock(n.name); if (nn != null) push(Iteration.it_Contaneer(nn.getList(0))); }; break;
                    case    Node.ID_NODE_SSET    : SetVal(n.name, n.Value); break;
                    case Node.ID_NODE_STRF: SetVal(n.name, string.Format(n.Value, n.readParams(_params))); break;
                    case Node.ID_NODE_STRFAS: SetVal(n.name, string.Format(_params.ContainsKey(n.Value) ? (string)_params[n.Value] : "param " + n.Value + "empty", n.readParams(_params))); break;
				    case	Node.ID_NODE_IF		 :break;				
				    default	: throw new Exception("Unknown Node Class found ("+n.Class+")");
			    };
		    };
	    }
	    //----------------------------------------------------------
	    private void ExecEXEC(Node n)
	    {
		    tool.ExecEXEC(n.Value,n.param,_params,conn);
	    }
	    //----------------------------------------------------------
	    private void ExecREFC(Node n)
	    {
		    tool.ExecREFC(n.Value,n.param,_params,conn);
	    }	
	    //----------------------------------------------------------
	    private void HandleCONTANEER(Iteration it)
	    {
		    if(it.childs.Count>it.step)
		    {
			    prepare_Node(it.childs[(int)it.step]);
			    it.step++;
		    }else pop();
	    }
	    //----------------------------------------------------------
	    private void HandleFETCH(Iteration it)
	    {
		    if(PrepareFields(it.rs,it.src.name))
		    {
			
			    it.step++;
                push(Iteration.it_Contaneer((it.step % 2 == 0) ? ((it.src.getList(1) != null) ? it.src.getList(1) : it.src.getList(0)) : it.src.getList(0)));
                if ((it.step == 1) && (it.src.getList(3) != null))
                    push(Iteration.it_Contaneer(it.src.getList(3)));
		    }else
		    {
			    pop();
                if ((it.step == 0) && (it.src.getList(2) != null))
                    push(Iteration.it_Contaneer(it.src.getList(2)));
		    }
	    }
	    //----------------------------------------------------------
	    private void HandleIteration(Iteration it)
	    {
		    if(it!=null)
		    {
			    switch(it.Class)
			    {
				    case	Iteration.ID_CONTANEER:HandleCONTANEER	(it);break;
				    case	Iteration.ID_FETCH	  :HandleFETCH		(it);break;
				    default: throw new Exception("unknown it.Class: "+it.Class); 
			    }
		    }else pop();
	    }
	    //----------------------------------------------------------
	    private StringBuilder runInner(Node n)
	    {
		    if (n.Class!=Node.ID_NODE_ROOT) throw new Exception("Node  must be is ROOT Class");
		    ROOT=n;
		    prepare_Node(n);
            while (stack.Count > 0)
                HandleIteration(stack.Peek());
		    return sb;
	    }
	    //----------------------------------------------------------	
        static internal StringBuilder run(Node n, Dictionary<String, Object> IN, StringBuilder sbb, interfaces.Connection conn, bool c)
	    {
		    if (n!=null)
		    {
			    Builder b= new Builder(IN,sbb,conn);
			    b._debug=c;
			    if(c) b.printParams();
			    return b.runInner(n); 
		    };
		    return null;
	    }
	    //----------------------------------------------------------
	    private void printParams(){
		    sb.Append("----------params\n");
		    if(sb!=null)
		    {
			    if(this._params!=null)
			    {
                    foreach (KeyValuePair<String, Object> kv in _params)
                        sb.AppendFormat("{0} == {1}\r\n",kv.Key,kv.Value);
			    }else
				    sb.Append("Params is Empty");
		    };
		    sb.Append("----------params end \n");
	    }
	    //----------------------------------------------------------
        static public String run(String templ, Dictionary<String, Object> IN, interfaces.Connection conn)
	    {
		       StringBuilder sb = new StringBuilder();		   
		       Parcer c= new Parcer();
		       Node n=c.ParceTemplate(templ);
		       if(n!=null)
		       {
			      // Node.Debug_prn(sb,n,true);
			       if(conn!=null)
			       {
				       Builder.run(n,IN,sb,conn,false);
			       }
			       else
				       throw new Exception("connection is null");
		       }else throw new Exception("Node  is null");		
		    return sb.ToString();
	    }
	    //----------------------------------------------------------	
    }
}

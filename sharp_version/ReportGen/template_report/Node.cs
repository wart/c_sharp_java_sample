﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rep4ead
{
    public class Node
    {

        internal const String tag_ROOT     = "ROOT"; 

	    internal const String tag_STATIC   = "STATIC";
        internal const String tag_PRN      = "PRN";
        internal const String tag_SINGLE   = "SINGLE";
        internal const String tag_SINGLE_AS= "SINGLEAS";

        internal const String tag_EXEC     = "EXEC";
        internal const String tag_REFC     = "REFC";

        internal const String tag_FETCH    = "FETCH";
        internal const String tag_FETCH_AS = "FETCHAS";


        internal const String tag_IF       = "IF";
        internal const String tag_BLOCK    = "BLOCK";
        internal const String tag_PUTBLOCK = "PUTBLOCK";

        internal const String tag_SSET     = "SSET";

        internal const String tag_ELSE     = "ELSE";
        internal const String tag_EMPTY    = "EMPTY";
        internal const String tag_ZEBRA    = "ZEBRA";
        internal const String tag_HEADER   = "HEADER";
        internal const String tag_END      = "END";
        internal const String tag_STRF     = "STRF";
        internal const String tag_STRFAS   = "STRFAS";

        internal const String tag_MULTI    = "MULTI";

 	    //===================================================
        internal const int ID_NODE_ROOT      = 0;
        internal const int ID_NODE_PRN       = 1;
        internal const int ID_NODE_SINGLE    = 2;
        internal const int ID_NODE_FETCH     = 3;
        internal const int ID_NODE_STATIC    = 4;
        internal const int ID_NODE_IF        = 5;
        internal const int ID_NODE_BLOCK     = 6;
        internal const int ID_NODE_PUTBLOCK  = 7;
        internal const int ID_NODE_SSET      = 8;
        internal const int ID_NODE_EXEC      = 9;
        internal const int ID_NODE_REFC      = 10;
        internal const int ID_NODE_SINGLEAS  = 11;
        internal const int ID_NODE_FETCHAS   = 12;
        internal const int ID_NODE_STRF      = 13;
        internal const int ID_NODE_STRFAS    = 14;


        internal const int ID_NCHILDS_ZEBRA  = 1;	//	childs1
        internal const int ID_NCHILDS_EMPTY  = 2;	//	childs2
        internal const int ID_NCHILDS_HEADER = 3;	//	childs3
        internal const int ID_NCHILDS_ELSE   = 4;	//	childs1 	
 	    private static int  prn_ofs = 4;
	    //--------------------------------------------------
        internal int Class;
        internal String name;
        internal String Value;
        private int chmode;
        internal List<Node>[] childs;
        internal List<String> param;
        //--------------------------------------------------
	    private Node()
	    {
            childs = null;
		    chmode=0;
		    param=null;
	    }
	    private Node(int cl)
	    {
		    Class=cl;
	    }
	    //--------------------------------------------------
        internal String ClassName()
	    {
		    switch(Class)
		    {
			    case	ID_NODE_ROOT	:return tag_ROOT; 
			    case	ID_NODE_PRN		:return tag_PRN;
			    case	ID_NODE_SINGLE	:return tag_SINGLE;
                case    ID_NODE_SINGLEAS: return tag_SINGLE_AS;
                case    ID_NODE_FETCH:   return tag_FETCH;
                case    ID_NODE_FETCHAS: return tag_FETCH_AS;
                case    ID_NODE_STATIC: return tag_STATIC; 
			    case	ID_NODE_IF		:return tag_IF;
			    case	ID_NODE_BLOCK	:return tag_BLOCK;
			    case	ID_NODE_PUTBLOCK:return tag_PUTBLOCK;
			    case	ID_NODE_SSET	:return tag_SSET;
			    case	ID_NODE_EXEC	:return tag_EXEC;
			    case	ID_NODE_REFC	:return tag_REFC;
                case    ID_NODE_STRF    :return tag_STRF;
                case    ID_NODE_STRFAS: return tag_STRFAS;
			
		    };
		    return "Unknown";
	    }
        //--------------------------------------------------
        internal bool allowBlockBuildInPlace()
	    {
		    if(Class==ID_NODE_BLOCK)
			    if (Value!=null)
				    return Value.Equals("true");
		    return false;
	    }
	    //--------------------------------------------------
	    private Node ParceParams(String prms)
	    {
            if (prms != null)
            {
                prms = prms.Trim();
                if (!prms.Equals(""))
                {
                    param = new List<String>();
                    foreach (string s in prms.Split(','))
                        param.Add(s.Trim());
                };
            }
		    return this;
	    }
	    //--------------------------------------------------
        private bool initlist(int mm)
        {
            if ((mm < 0) || (mm > 3)) return false;
            if (childs == null) childs = new List<Node>[4];
            if (childs[mm] == null) childs[mm] = new List<Node>();
            return true;
        }
	    //--------------------------------------------------
        internal List<Node> getList(int nn)
        {
            if ((nn < 0) || (nn > 3)||(childs == null)) return null;
            return childs[nn];
        }
        //--------------------------------------------------
        internal void AddNode(Node n)
	    {
            if (n != null)
                if (initlist(chmode))
                    childs[chmode].Add(n);
	    }
	    //--------------------------------------------------
        internal void RootAddNamedBlock(Node n)
	    {
		    if(Class==ID_NODE_ROOT)
                if(initlist(2))
				    childs[2].Add(n);
	    }
	    //--------------------------------------------------
        internal Node RootGetNamedBlock(String nam)
	    {
		    if(Class==ID_NODE_ROOT)
			    if(childs!=null)
                    if (childs[2] != null)
                        return childs[2].First(p =>(p.name!=null)&&(p.name.Equals(nam)));
		    return null;	
	    }
        //--------------------------------------------------
        internal object[] readParams(Dictionary<String, Object> args)
        {
            object[] ret = new object[param.Count];
            for (int i = 0; i < param.Count; ++i)
                if(args.ContainsKey(param[i]))
                    ret[i]=args[param[i]];

                return ret;
        }
        //==================================================
        internal bool allowAndSwitchCh2(int mm)
	    {
            if ((mm < 1) || (mm > 3)) return false;
            if (childs == null) return true;
		    switch(Class)
		    {
                case ID_NODE_FETCH:                            if (childs[mm] == null) { chmode = mm; return true; }; break;
                case ID_NODE_IF   : if (mm == ID_NCHILDS_ELSE) if (childs[ 1] == null) { chmode =  1; return true; }; break;
		    };
		    return false;
	    }
	    //==================================================
        static internal Node NodeRoot()
	    {
		    return new Node(ID_NODE_ROOT);
	    }
	    //--------------------------------------------------
        static internal Node initname(int cl, String[] s)
        {
            if (s.Length > 1)
                if(s[1]!=null)
                    if(s[1].Length>0)
                    {
                        Node n = new Node(cl);
                        n.name = s[1];
                        return n;
                    }
            return null;
        }
	    //--------------------------------------------------
        static internal Node initp3(int cl, String[] s,int needval=1)
        {
            Node n = new Node(cl);
            if(s!=null)
                if (s.Length > 2)
                {
                    n.name = s[1];
                    n.Value = s[2];
                    if ((n != null)&& (n.name == null) || (n.name.Length == 0)) n = null;
                    if ((n != null)&& (needval ==1) && ((n.Value == null) || (n.Value.Length == 0))) n = null;
                    if ((n != null)&& (needval == 2)) if (n.Value != null) n.Value = n.Value.ToUpper().Trim();
                    if ((n != null)&&(s.Length > 3)) n = n.ParceParams(s[3]);
                    return n;
                }
            return null;
        }
        //--------------------------------------------------
        static internal Node initp2v(int cl, String[] s)
        {
            Node n = new Node(cl);
            if (s != null)
                if (s.Length > 1)
                {
                    n.Value = s[1];
                    if (s.Length > 2) n = n.ParceParams(s[2]);
                    if (n != null)
                        if ((n.Value == null) || (n.Value.Length == 0))
                            n = null;
                    return n;
                }
            return null;
        }
        //--------------------------------------------------
        static internal Node NodeStatic(String s)
        {
            Node n = new Node(ID_NODE_STATIC);
            n.Value = s;
            return n;
        }
        //==================================================
	    private static void prn				(StringBuilder sbb,int ofs,String txt,bool ln=true)
	    {
		    if(sbb==null)
		    {
		        for(int i=0;i<ofs;i++)
                    Console.Write(" ");
                Console.Write(txt);
                if (ln) Console.WriteLine("");
		    }
            else
		    {
			    for(int i=0;i<ofs;i++)
				    sbb.Append(" ");
                sbb.Append(txt);
                if (ln) sbb.Append("\r\n");
		    }
	    }
	    //-------------------------------------------------
	    private void DPPP	(StringBuilder sbb,int ofs)
	    {
		    if(param!=null)
		    {
			    prn(sbb,ofs,"Params:");
			    int sz=param.Count;
			    for(int i=0;i<sz;i++)
				    prn(sbb,ofs+prn_ofs,param[i]);
		    }else prn(sbb,ofs,"Params: Empty(null)");
	    }
	    //--------------------------------------------------
	    private void DPCH(StringBuilder sbb,int ofs,int listid,String cap,bool exclstatic)
	    {
            List<Node> ch = null;
            if (childs != null) ch=childs[listid];
		    if(ch!=null)
		    {
			    int sz=ch.Count;
			    prn(sbb,ofs,cap+"  ("+sz+"):");
			
			    Node n=null;
			    for(int i=0;i<sz;i++)
				    if((n=ch[i])!=null)
				    {
					    n.Debug_prn(sbb,ofs+prn_ofs,exclstatic);
				    }else prn(sbb,ofs+prn_ofs,"null child found");
		    }else prn(sbb,ofs,cap+": Empty(null)");
	    }
	    //--------------------------------------------------
        private delegate void _PN(string N);
	    private void Debug_prn			(StringBuilder sbb,int ofs,bool exclstatic)
	    {
            if(!(exclstatic && ID_NODE_STATIC==Class))   prn(sbb,ofs,"Node Class: "+ClassName(),false);
            _PN PN = (string N) => { prn(sbb, ofs, N+ ": '" + name + "'"); };
            _PN PV = (string N) => { prn(sbb, ofs, N+ ": '" + Value + "'"); };

            switch (Class)
		    {
                case ID_NODE_ROOT:       DPCH(sbb, ofs, 0, "Childs", exclstatic);
                                         DPCH(sbb, ofs, 2, "Named Blocks", exclstatic);
                                         break;
                case ID_NODE_PRN:        PN("FIELD");			    break;
			    case ID_NODE_PUTBLOCK:   PN("BLOCK");			    break;
                case ID_NODE_BLOCK:      PN("Name");  PV("Visibility");
                                         DPCH(sbb, ofs, 2, "Childs", exclstatic);
			    break;						
			    case ID_NODE_SINGLE:     PN("NAME");  PV("SQL");   DPPP(sbb, ofs);break;
                case ID_NODE_SINGLEAS:   PN("NAME");  PV("PRMSQL"); DPPP(sbb, ofs); break;
                case ID_NODE_SSET:       PN("Param"); PV("Value"); break;
                case ID_NODE_FETCH:      PN("NAME");  PV("SQL");   DPPP(sbb, ofs);
                                         DPCH(sbb, ofs, 3, "Header", exclstatic);
                                         DPCH(sbb, ofs, 0, "Childs", exclstatic);
                                         DPCH(sbb, ofs, 1, "Zebra", exclstatic);
                                         DPCH(sbb, ofs, 2, "Empty", exclstatic);
			    break;
                case ID_NODE_FETCHAS:    PN("NAME"); PV("PRMSQL"); DPPP(sbb, ofs);
                                         DPCH(sbb, ofs, 3, "Header", exclstatic);
                                         DPCH(sbb, ofs, 0, "Childs", exclstatic);
                                         DPCH(sbb, ofs, 1, "Zebra", exclstatic);
                                         DPCH(sbb, ofs, 2, "Empty", exclstatic);
                break;
                case ID_NODE_IF:    // TODO:
                                         DPCH(sbb, ofs, 0, "True Childs", exclstatic);
                                         DPCH(sbb, ofs, 1, "Else Childs", exclstatic);
                break;
			    case ID_NODE_STATIC:     if(!exclstatic){PV("text");}else prn(sbb,ofs,"...");break;
                case ID_NODE_EXEC:       PV("SQL");  DPPP(sbb, ofs);break;
                case ID_NODE_REFC:       PV("SQL");  DPPP(sbb, ofs);break;
                case ID_NODE_STRF:       PN("NAME"); PV("FORMAT"); DPPP(sbb, ofs); break;
                case ID_NODE_STRFAS:     PN("NAME"); PV("FORMAT PARAM"); DPPP(sbb, ofs); break;
                default: prn(sbb, ofs, "Wrong Node class " + Class); break;
            };
            prn(sbb, 0, "");
		
	    }
	    //--------------------------------------------------	
        internal static void Debug_prn(StringBuilder sbb, Node n, bool exclstatic)
	    {
		       if(n!=null)
		       {
			       n.Debug_prn(sbb,0,exclstatic);
		       }else
			       prn(sbb,0,"Node is null");
		   
	    }
	//--------------------------------------------------	




    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rep4ead
{
    namespace interfaces
    {
        // -------------------------------------------------------------
        public class ReportData
        {
            public string content_type;
            public string body;
            public List<object> prms;
        }

        /*
        public abstract class TemplateSource
        {
            public abstract ReportData getData(string name);
            public abstract List<ReportDesc> getReportList();
//            public abstract bool InitParameters(Dictionary<String, Object> dst, List<ParameterDesc> src, mcpr4ead.console.Engine engine);
        }/**/
        //------------------------------------------------------------
        public abstract class ResultSet
        {
            public abstract object Get(int id);
            public abstract string Name(int id);
            public abstract int Count();
            public abstract bool Next();
            public abstract void Dispose();
        }
        //------------------------------------------------------------
        public abstract class Connection
        {
            public abstract Statement CreateStatement(string sql);
        }
        //------------------------------------------------------------
        public abstract class Statement
        {
            public abstract void SetParam(string name, object value);
            public abstract ResultSet ExecuteReader();
        }
        //------------------------------------------------------------
    }
}

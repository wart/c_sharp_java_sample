﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace rep4ead
{
    //------------------------------------------------------------
    public class tool
    {
        //--------------------------------------------------------
        internal static Dictionary<String, Object> CopyDictionary(Dictionary<String, Object> src, Dictionary<String, Object> dst, String pref)
	    {
            if (src != null)
            {
                if (dst == null)
                   dst = new Dictionary<String, Object>();
                foreach (var kv in src)
                {
                    dst.Add(pref + kv.Key, kv.Value);
                }
                return dst;
            }
            return null;
	    }
        // ---------------------------------------------------------
        internal static bool check_names(string[] names)
        {
            if (names != null)
            {
                int sz = names.Length;
                if (sz > 1)
                {
                    for (int c = 0; c < sz - 1; c++)
                        for (int i = c; i < sz - 1; i++)
                            if (names[i].Equals(names[i + 1]))
                                return false;
                    return true;
                }
            }
            return false;
        }
	    //--------------------------------------------------------
        internal static interfaces.Statement PrepareStatement(String sql, List<String> n, Dictionary<String, Object> _params, interfaces.Connection conn, StringBuilder sb, bool debug)
        {
            if (debug && (sb != null))
            {
                sb.Append("--------------------\n" + sql + "\n");
                sb.Append("Size: " + n.Count + "\n");
                foreach(string nm in n)
                    sb.Append(String.Format("{0} == {1} \n", nm, _params[nm]));
                sb.Append("------------\n");
            };
            interfaces.Statement stmt = conn.CreateStatement(sql);
            int pid = 1;
            if(n!=null)
                foreach (string prmname in n)
                    if (_params.ContainsKey(prmname))
                        stmt.SetParam(":" + (pid++), _params[prmname]);
            return stmt;
        }
        //--------------------------------------------------------
        internal static bool PrepareFields(interfaces.ResultSet rs, String pref, Dictionary<String, Object> dst)
	    {
            if ((rs != null)&& (dst != null))
		    {
                if (rs.Next())
                {
                    for (int i = 0; i < rs.Count(); i++)
                    {
                        string k=pref + rs.Name(i);
                        if (!dst.ContainsKey(k))
                            dst.Add(k, rs.Get(i));
                        else dst[k] = rs.Get(i);
                    };
                    return true;
                }
                else
                    rs.Dispose();
            }
            return false;
        }
	    //----------------------------------------------------------

        internal static interfaces.ResultSet init_request(String sql, List<String> n, Dictionary<String, Object> _params, interfaces.Connection conn, StringBuilder sb = null, bool debug = false)
	    {
            interfaces.ResultSet rs = null;
            interfaces.Statement st = PrepareStatement(sql, n, _params, conn, sb, debug);
            try
            {
                rs=st.ExecuteReader();
            }
            catch (Exception ex)
            {
                if (sb != null) sb.AppendFormat("{0} {1}", sql ,ex.Message);
            };
		    return rs;
	    }
	    //----------------------------------------------------------
         /**/
        /*
	    private static void Setprm(int num, Object pval,CallableStatement st)
	    {
		    if(pval!=null)
		    {
			    String cn= pval.getClass().getSimpleName();			
			    if (cn.Equals("String" )){st.setString(num, (String) pval);};
                if (cn.Equals("Integer")) { st.setInt(num, (Integer)pval); };
                if (cn.Equals("Double")) { st.setDouble(num, (Double)pval); };
                if (cn.Equals("Long")) { st.setLong(num, (Long)pval); };
                if (cn.Equals("Float")) { st.setFloat(num, (Float)pval); };
                if (cn.Equals("Date")) { st.setDate(num, (Date)pval); };			
		    }else
			    st.setNull(num+1,java.sql.Types.NULL);
	    }/**/
	    //----------------------------------------------------------

        internal static void ExecEXEC(String sql, List<String> n, Dictionary<String, Object> _params, interfaces.Connection conn)
	    {
         //   OracleCommand oc = PrepareCommand(sql, n, _params, conn, null, false);

         //   oc.Parameters.Add(":rett", OracleDbType.Varchar2, 2000, ParameterDirection.ReturnValue);
         //   oc.ExecuteNonQuery();
            /*
		    st.executeQuery();
		
		    for(int num=1;num<=cnt;num++)
		    {
			    String prm;
			    if ((prm=n[num-1])!=null)
				    switch(dd.getParameterMode(num))
				    {
					    case ParameterMetaData.parameterModeInOut:
					    case ParameterMetaData.parameterModeIn:
						    Object o=null;
						    switch(dd.getParameterType(num))
						    {
							    case java.sql.Types.BIGINT :o=st.getLong   (num);break;
							    case java.sql.Types.NUMERIC:o=st.getLong   (num);break;
							    case java.sql.Types.VARCHAR:o=st.getString (num);break;
							    case java.sql.Types.BOOLEAN:o=st.getBoolean(num);break;
						    };
						    _params.Add(prm,o);
					    break;
				    };
		    }
            /**/
	    }
	    //----------------------------------------------------------

        internal static void ExecREFC(String sql, List<String> param, Dictionary<String, Object> _params,
                interfaces.Connection conn) 
	    {
        /*    
		    ResultSet rs=null;
		    if(conn!=null)
		    {
			    CallableStatement ps = conn.prepareCall("begin  ?:="+sql+";end;");
			    ps.registerOutParameter(1, OracleTypes.CURSOR);			
			    if(param!=null)
			    {
				    String prm;
				    Object pval=null;
				    int sz=param.Count;
				    for (int i=0;i<sz;i++)
					    if ((prm=param[i])!=null)
					    {
						    pval=_params[prm];
						    if(pval!=null)
						    {
							    String cn= pval.getClass().getSimpleName();			
							    if (cn.Equals("String" )){ps.setString(i+2, (String) pval);};
							    if (cn.Equals("Integer")){ps.setInt   (i+2, (Integer)pval);};
							    if (cn.Equals("Double" )){ps.setDouble(i+2, (Double) pval);};
							    if (cn.Equals("Long"   )){ps.setLong  (i+2, (Long)   pval);};
							    if (cn.Equals("Float"  )){ps.setFloat (i+2, (Float)  pval);};
							    if (cn.Equals("Date"   )){ps.setDate  (i+2, (Date)   pval);};			
						    }else
							    ps.setNull(i+2,java.sql.Types.NULL);
					    }
			    }
			    ps.execute(); 
			    rs = (ResultSet) ps.getObject(1);
			    if(rs!=null)
				    while (rs.next())
				    {
					    _params.Add(rs.getString(1),rs.getString(2));
					    //System.out.println(rs.getString(0)); 
				    } 
		    };
         /**/   
	    }
	    //------------------------------------------------------------------

    }
}

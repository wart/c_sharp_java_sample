﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
namespace rep4ead
{
    namespace Oracle
    {
        //------------------------------------------------------------------------
        public class ResultSet : rep4ead.interfaces.ResultSet
        {
            internal OracleDataReader odr;
            public override object Get(int id)
            {
                return odr[id];
            }
            public override string Name(int id)
            {
                return odr.GetName(id);
            }
            public override int Count()
            {
                return odr.FieldCount;
            }
            public override bool Next()
            {
                return odr.Read();
            }
            public override void Dispose()
            {
                odr.Close();
                odr.Dispose();
            }
        }
        //------------------------------------------------------------------------
        public class Statement : rep4ead.interfaces.Statement
        {
            internal OracleCommand ocmd;
            //--------------------------------------------------------------------
            public override void SetParam(string name, object value)
            {
                if (value != null)
                {
                    if (value.GetType().Name.Equals("String"))
                    {
                        ocmd.Parameters.Add(name, OracleDbType.Varchar2, ((string)value).Length, value, ParameterDirection.Input);
                    }
                    else
                    {
                        ocmd.Parameters.Add(name, OracleDbType.Decimal, value, ParameterDirection.Input);
                    }
                }
            }
            //--------------------------------------------------------------------
            public override rep4ead.interfaces.ResultSet ExecuteReader()
            {
                ResultSet rs = new ResultSet();
                rs.odr = ocmd.ExecuteReader();
                return rs;
            }
        }
        // ------------------------------------------------------------------------
        public class Connection : rep4ead.interfaces.Connection
        {
            internal OracleConnection oc;
            //--------------------------------------------------------------------
            public override rep4ead.interfaces.Statement CreateStatement(string sql)
            {
                Statement st = new Statement();
                st.ocmd = oc.CreateCommand();
                st.ocmd.CommandText = sql;
                return st;
            }
            //--------------------------------------------------------------------
            public static rep4ead.interfaces.Connection Create(string connstr)
            {
                Connection c = new Connection();
                c.oc = new OracleConnection(connstr);
                c.oc.Open();
                return c;
            }
            //--------------------------------------------------------------------
            public static rep4ead.interfaces.Connection Create(OracleConnection conn)
            {
                Connection c = new Connection();
                c.oc = conn;
                return c;
            }
            //--------------------------------------------------------------------
        }
        //------------------------------------------------------------------------
      /*  public class TemplateSource : rep4ead.interfaces.TemplateSource
        {
            internal OracleConnection oc;
            internal string sql;
            internal string sql2;
            internal string sql3;
            // -------------------------------------------------------------------
            public override rep4ead.interfaces.ReportData getData(string name)
            {
                // select name, typ, args, descr from t_rep_params where rep=:name

                rep4ead.interfaces.ReportData ret = null;
                OracleCommand ocmd = oc.CreateCommand();
                ocmd.CommandText = sql;
                ocmd.Parameters.Add(":name", OracleDbType.Varchar2, ((string)name).Length, name, ParameterDirection.Input);
                OracleDataReader odr=ocmd.ExecuteReader();
                if (odr.Read() && odr.FieldCount > 0)
                {
                    ret = new interfaces.ReportData();
                    ret.body = odr.GetString(0);
                    ret.content_type = odr.GetString(1);
                    ret.prms = new List<ParameterDesc>();
                    OracleCommand ocmd1 = oc.CreateCommand();
                    ocmd1.CommandText = sql2;
                    ocmd1.Parameters.Add(":name", OracleDbType.Varchar2, ((string)name).Length, name, ParameterDirection.Input);
                    OracleDataReader odr2 = ocmd1.ExecuteReader();
                    if (odr2.FieldCount > 5)
                        while (odr2.Read())
                        {
                            //name, typ, caption, arg, linkprm
                            ret.prms.Add(new ParameterDesc(odr2.GetString(0), RG_PH.PT((int)odr2.GetDecimal(1)), 
                                                           odr2.IsDBNull(2) ? "" : odr2.GetString(2), 
                                                           odr2.IsDBNull(3) ? "" : odr2.GetString(3), 
                                                           odr2.IsDBNull(4) ? "" : odr2.GetString(4), 
                                                           odr2.IsDBNull(5) ? "" : odr2.GetString(5)));
                        };
                    odr2.Close();
                    odr2.Dispose();
                    ocmd1.Dispose();
                };
                odr.Close();
                odr.Dispose();
                ocmd.Dispose();
                return ret;
            }
            // -------------------------------------------------------------------
            
            public override List<RGI.ReportDesc> getReportList()
            {
                List<RGI.ReportDesc> ret = null;
                OracleCommand ocmd = oc.CreateCommand();
                if (ocmd != null)
                {
                    ocmd.CommandText = sql3;
                    OracleDataReader odr = ocmd.ExecuteReader();
                    if (odr != null)
                    {
                        if (odr.FieldCount == 2)
                        {
                            ret = new List<ReportDesc>();
                            while (odr.Read())
                            {
                                RGI.ReportDesc rd = new ReportDesc(odr.GetString(0), odr.GetString(1));
                                if (rd != null)
                                    ret.Add(rd);
                            }
                        };
                        odr.Close();
                    }
                    ocmd.Dispose();
                };
                return ret;
            }
            // -------------------------------------------------------------------
            public TemplateSource(OracleConnection occ, string _sql, string _sql2, string _sql3)
            {
                oc = occ;
                sql = _sql;
                sql2 = _sql2;
                sql3 = _sql3;
            }
        }/**/
        //------------------------------------------------------------------------
    }
}

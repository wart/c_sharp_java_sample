﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using System.IO;
using Oracle.DataAccess.Types;
using rep4ead;
using System.Threading;
using System.Diagnostics;
using mcpr4ead;

namespace RNG.helpers
{
    // -------------------------------------------------------
    public class consts
    {
        public const string calling_args_namespace_name = "callargs";
    };

    public class SQL
    {
        public static string ModelDesc      = "select ID, ISCORP, ISCARD, CAP  from TRG_REPORT where NAME=:1";
        public static string ModelSources   = "select GLOBAL_ID, PARAM_LIST, generator, CONTENT_TYPE, FILE_EXT, ACTUAL_BEGIN,ACTUAL_END,format,arg from TRG_SOURCE where REP_ID=:1";
        public static string ParamListDesc  = "select name from TRG_PLISTS where id=:1";
        public static string ParamDesc      = "Select NAME,TYPE,CAPTION,ARG,LINKPRM,DEF_VAL from TRG_PDESCS where ID=:1";
        public static string ParamListItems = "select pr_id from TRG_PLITEMS where pl_id=:1";
        public static string ReportList     = "select name,cap from TRG_REPORT";
        public static string ReportListId = "select name,cap from TRG_REPORT where id in(select r_id from TRG_REP_LITEMS where l_id=:1)";
//        public static string ReportListCard = "select name,cap from TRG_REPORT where iscard='Y'";
        public static string SourceSource   = "select source  from TRG_SOURCE where GLOBAL_ID=:1";
        public static string Billing_ID_List = "select billing_id||'',periodname from o1.t_bill_legend t where billing_id>0 and successkey='Y' order by  billing_id desc";
    };
    // -------------------------------------------------------
    public enum ParamType
    {
        Number = 0x00,
        String = 0x01,
        Date = 0x02,
        Flag = 0x03,
        List = 0x04,
        MultiSet = 0x05,
        MicroCmd = 0x06
    }
    // -------------------------------------------------------
    internal enum GeneratorType
    {
        RDF = 0x00,
        TML = 0x01
    }
    // ------------------------------------------------------
    class toENUM
    {
        // ---------------------------------------------------
        public static GeneratorType GT(int id)
        {
            switch (id)
            {
                case 0: return GeneratorType.RDF;
                case 1: return GeneratorType.TML;
            };
            return GeneratorType.RDF;
        }
        // ---------------------------------------------------
        public static ParamType PT(int id)
        {
            switch (id)
            {
                case 0: return ParamType.Number;
                case 1: return ParamType.String;
                case 2: return ParamType.Date;
                case 3: return ParamType.Flag;
                case 4: return ParamType.List;
                case 5: return ParamType.MultiSet;
                case 6: return ParamType.MicroCmd;
            };
            return ParamType.String;
        }
        // ---------------------------------------------------
    }
    // -------------------------------------------------------
    public class ReportDesc
    {
        public string name { get; set; }
        public string caption { get; set; }
        public ReportDesc(string _name, string _caption)
        {
            name = _name;
            caption = _caption;
        }
    }
    // -------------------------------------------------------
    public class ParameterDesc
    {
        // ---------------------------------------------------
        public class Set
        {
            public string value { get; set; }
            public string caption { get; set; }
            public Set() { }
        }
        public string name { get; set; }
        public ParamType type { get; set; }
        public string caption { get; set; }
        public string arg { get; set; }
        public string lprm { get; set; }
        public string value { get; set; }
        public List<Set> sets { get; set; }
        // ---------------------------------------------------
        public ParameterDesc() { }
        public ParameterDesc(string _name, ParamType _type, string _caption, string _arg, string _lprm, string _value)
        {
            name = _name;
            type = _type;
            caption = _caption;
            arg = _arg;
            lprm = _lprm;
            value = _value;
        }
        // ---------------------------------------------------
    }
    // -------------------------------------------------------
    internal class ParameterList
    {
        // ---------------------------------------------------
        internal int id;
        internal string name;
        internal List<ParameterDesc> Params;
        // ---------------------------------------------------
        internal ParameterList(int _id, string _name)
        {
            id = _id;
            name = _name;
            Params = null;
        }
        // ---------------------------------------------------
    }
    // ===================================================
    //  DB Base Works
    // ===================================================
    public class Select
    {
        //------------------------------------------------
        private static OracleConnection _oraconn;
        internal static OracleConnection oraconn
        {
            get { return _oraconn; }
            set
            {
                if(value!=null)
                {
                    _oraconn = value;
                    conn = rep4ead.Oracle.Connection.Create(_oraconn);
                }
            }

        }
        //------------------------------------------------
        internal static rep4ead.interfaces.Connection conn;
        //------------------------------------------------
        internal Select() { }
        //------------------------------------------------
        public string sql;
        public OracleCommand cmd;
        public OracleDataReader odr;
        public static Select Exec(string sql, params object[] args)
        {
             if (oraconn != null)
             {
                 Select s = new Select();
                 s.cmd = oraconn.CreateCommand();
                 if (s.cmd != null)
                 {
                    s.cmd.CommandText = sql;
                    s.cmd.CommandType = CommandType.Text;
                    if (args != null)
                      for (int i = 0; i < args.Length; ++i)
                         if (args[i].GetType().Name.Equals("String"))
                         {
                             s.cmd.Parameters.Add(":" + (i + 1), OracleDbType.Varchar2, ((string)args[i]).Length, args[i], ParameterDirection.Input);
                         }
                         else
                         {
                             s.cmd.Parameters.Add(":" + (i + 1), OracleDbType.Decimal, args[i], ParameterDirection.Input);
                         }
                     s.odr = s.cmd.ExecuteReader();
                     if (s.odr!=null)
                        return s;
                 }
             }
             return null;
        }
        // ---------------------------------------------------
        public void Release()
        {
            if (odr != null)
            {
                odr.Close();
                odr.Dispose();
            };
            if (cmd != null)
                cmd.Dispose();
        }
        // ---------------------------------------------------
    }
    // ---------------------------------------
    public class helper
    {
        // ---------------------------------------------------
        public static string prepDate(string s)
        {
            string[] sa = s.Split('/');
            if (sa[0].Length < 2) sa[0] = '0' + sa[0];
            if (sa[1].Length < 2) sa[1] = '0' + sa[1];
            s = string.Join("/", sa);
            return s;
        }
        // ---------------------------------------------------
        internal static byte[] getSource(int gid)
        {
            byte[] ret=null;
            var s=Select.Exec(SQL.SourceSource, gid);
            if (s != null)
            {
                if (s.odr.Read())
                {
                    var ob = s.odr.GetOracleBlob(0);
                    ret = new byte[ob.Length];
                    ob.Read(ret, 0, (int)ob.Length);
                }
                s.Release();
            }
            return ret;
        }
        // ---------------------------------------------------
        public static string getMultisetVal(List<ParameterDesc.Set> prm, List<ParameterDesc.Set> desc)
        {
            if ((prm == null) || (desc == null)) return "";
            if (prm.Count != desc.Count) return "";
            string ret = ""; int i = 0;
            for (int j = 0; j < desc.Count; ++j)
            {
                if (desc[j].value.Equals(prm[j].value))
                {
                    if (i > 0) ret += ","; else i = 1;
                    ret += prm[j].value;
                };
            }
            return ret;
        }
        // ------------------------------------------------------
        public static void FillMultiSet(ParameterDesc pd)
        {
            if ((pd != null) && (pd.type == ParamType.MultiSet) && (pd.sets == null))
            {
                var s = Select.Exec(pd.arg);
                if (s != null)
                {
                    pd.sets = new List<ParameterDesc.Set>();
                    while (s.odr.Read())
                    {
                        var sli = new ParameterDesc.Set();
                        sli.value = s.odr.GetString(0);
                        sli.caption = s.odr.GetString(1);
                        pd.sets.Add(sli);
                    };
                    s.Release();
                }
            }
        }
        // ------------------------------------------------------
        internal static void Ap(Dictionary<string, object> dest, string key, object val)
        {
            if (dest.ContainsKey(key))
                dest[key] = val;
            else
                dest.Add(key, val);
        }
        // ------------------------------------------------------
        public static void eng_add_call_args(mcpr4ead.console.Engine eng, string name, Dictionary<string, object> vars)
        {
            if (eng == null) return ;
            if (name == null) return ;
            if (vars == null) return ;
            console_implements.Folder f = (console_implements.Folder)eng.Add_Folder(name, mcpr4ead.console.ItemAttr.Sys);
            if (f != null)
                foreach (var v in vars)
                    if((v.Key!=null)&&(v.Value!=null))
                        eng.Add_Variable(v.Key, v.Value.ToString(), "", mcpr4ead.console.ItemAttr.Sys, f);
        }
        // ------------------------------------------------------
        public static Dictionary<string, object> FillParams(mcpr4ead.console.Engine eng, List<ParameterDesc> desc, List<ParameterDesc> src, Dictionary<string, object> dest = null)
        {

            if (eng == null) return null;
            if (src == null) return null;
            if (dest == null) dest = new Dictionary<string, object>();
            foreach (var pd in src)
            {
                ParameterDesc dc = desc.First(d => d.name.Equals(pd.name));
                if (dc != null)
                    switch (dc.type)
                    {
                        case ParamType.Number:
                        case ParamType.String:
                        case ParamType.Date:
                        case ParamType.List:
                            try
                            {
                                Ap(dest, pd.name, pd.value);
                            }
                            catch (Exception ex) { }
                            break;
                        case ParamType.Flag:
                            Ap(dest, pd.name, pd.value.Equals(dc.value) ? dc.value : "");
                            break;
                        case ParamType.MultiSet:
                            try
                            {
                                Ap(dest, pd.name, getMultisetVal(pd.sets, dc.sets));
                            }
                            catch (Exception ex) { }
                            break;
                        case ParamType.MicroCmd:
                            break;
                    }
            }
            foreach (var pd in desc)   if (!dest.ContainsKey(pd.name))    dest.Add(pd.name, pd.value);

            if (eng != null)
            {
                eng_add_call_args(eng, consts.calling_args_namespace_name, dest);
                foreach (var pd in desc)
                    if (pd.type == ParamType.MicroCmd)
                        Ap(dest, pd.name, eng.ExecS(pd.arg));
                eng.DelItem(consts.calling_args_namespace_name);
            };
            return dest;
        }
        // ---------------------------------------------------
    }
    // -------------------------------------------------------
    public class Module
    {
        public static void init(OracleConnection oc, Dictionary<string, string> cp, string ep, string rp, string gp)
        {
            Select.oraconn=oc;
            RDF.InitParams(cp,ep,rp,gp);
        }
    }
    // -------------------------------------------------------
    internal class RDF
    {
        private const string cmdArgsFormat = @"module={0} userid={1} mode=bitmap printjob=no paramform=no destype=file desformat={2} desname={3} {4}";
        // ---------------------------------------------------
        internal static Dictionary<string,string> connectParam;
        internal static string execPath;      // rwrunName
        internal static string ReportPath;
        internal static string GenPath;
        // ---------------------------------------------------
        internal static void InitParams(Dictionary<string,string> cp, string ep, string rp, string gp)
        {
            connectParam = cp;
            execPath = ep;
            ReportPath = DirectoryInit(rp);
            GenPath = DirectoryInit(gp);
        }
        // ---------------------------------------------------
        internal static string DirectoryInit(string path)
        {
            string result = Path.GetFullPath(path);
            if (!Directory.Exists(result))
            {
                Directory.CreateDirectory(result);
            }
            return result;
        }
        // ---------------------------------------------------
        internal static string buildParams(string name, string src_appendix, string appendix, string format, string repArgs, string commprm)
        {
            // 0 - reportsPath + toGen[i].Name,
            // 1 - svvo/svao7@nostart
            // 2 - model.Format,
            // 3 - generatedFileName,
            // 4 - GetParamString(toGen[i])
            return string.Format(cmdArgsFormat, ReportPath + name + src_appendix,
                                                commprm,
                                                format,
                                                GenPath + name + appendix + "." + format,
                                                repArgs);
        }
        // ---------------------------------------------------
        internal static bool InitSource(Report rep)
        {
            if (rep != null)
                if(rep.src_init==false)
                {
                    var dt=helper.getSource((int)rep.gid);
                    if(dt!=null)
                    {
                        string fn = ReportPath + rep.name + "_" + rep.gid + ".rdf";
                        if (File.Exists(fn))
                        {
                            if (new FileInfo(fn).Length != dt.Length)
                               File.WriteAllBytes(fn, dt);
                        }
                        else
                            File.WriteAllBytes(fn, dt);
                        rep.src_init = true;
                        return true;
                    }
                }
                else
                    return true;
            return false;
        }
        // ---------------------------------------------------
        private class builder
        {
            internal Result res;
            internal string cpn;
            internal void run()
            {
                string sargs = "";
                foreach (var key in res.args.Keys)
                {
                    sargs += key + "=" + res.args[key].ToString() + " ";
                }
                string append_name = Thread.CurrentThread.ManagedThreadId + "_" + DateTime.Now.ToString("yy-MM-dd-hh-mm-ss-fff");
                ProcessStartInfo psi = new ProcessStartInfo(execPath, buildParams(res.rep.name,"_" + res.rep.gid, append_name, res.rep.format, sargs,
                    
                    connectParam.ContainsKey(cpn)?connectParam[cpn]:""));
                psi.WorkingDirectory = ReportPath;
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.UseShellExecute = true;
                Process proc = new Process();
                proc.StartInfo = psi;
                proc.Start();
                proc.WaitForExit();
                string fn =GenPath +res.rep.name+ append_name + ".pdf";
                res.sw =File.OpenRead(fn);
                res=null;
            }
        }
        // ---------------------------------------------------
        internal static Result Build(Report rep, Dictionary<string, object> Args, string arg)
        {
            if(InitSource(rep))
            {

                builder b = new builder(); b.cpn = arg;
                Result r = b.res = new Result(rep, Args);
                r.oThread = new Thread(new ThreadStart(b.run));
                r.oThread.Start();
                return r;
            }
            return null;
        }
        // ---------------------------------------------------
    }
    // -------------------------------------------------------
    internal class TML
    {
        // ---------------------------------------------------
        internal static bool InitSource(Report rep)
        {
            if (rep != null)
            {
                if ((rep.src_init == false) || (rep.body == null))
                {
                    var dt=helper.getSource((int)rep.gid);
                    if(dt!=null)
                    {
                        Encoding enc = Encoding.GetEncoding(rep.format);
                        char[] c = new char[enc.GetCharCount(dt, 0, dt.Length)];
                        enc.GetChars(dt, 0, dt.Length, c, 0);
                        string str = new string(c);
                        Parcer p = new Parcer();
                        Node n = p.ParceTemplate(str);
                        if (n != null)
                        {
                            rep.body=n;
                            return true;
                        }
                    }
                }else
                    return true;
            }
            return false;
        }
        // ---------------------------------------------------
        private class builder
        {
            internal Result res;
            internal void run()
            {
                StringBuilder sb = new StringBuilder();
                Builder.run((Node)res.rep.body, res.args, sb, Select.conn, false);
                res.sw = new System.IO.MemoryStream();
                var a=Encoding.GetEncoding(res.rep.format).GetBytes(sb.ToString());
                res.sw.Write(a, 0, a.Length);
                res.sw.Seek(0, SeekOrigin.Begin); 
                res=null;
            }
        }
        // ---------------------------------------------------
        internal static Result Build(Report rep, Dictionary<string, object> Args)
        {
            if(InitSource(rep))
            {
                builder b= new builder();
                Result r=b.res=new Result(rep,Args);
                r.oThread= new Thread(new ThreadStart(b.run));
                r.oThread.Start();
                return r;
            }
            return null;
        }
        // ---------------------------------------------------
    }
    // -------------------------------------------------------
}

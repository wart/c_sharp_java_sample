﻿
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Data;
using System.IO;
using System.Configuration;
using Oracle.DataAccess.Client;
using OperPO.Tools;

// -------------------------------------------
public class RG_wlad
{
    // ---------------------------------------
    public class ReportBuildParams
    {
        public string name;
        public string bid;
        public List<RNG.helpers.ParameterDesc> param;
        public ReportBuildParams() { }
        public ReportBuildParams(List<RNG.helpers.ParameterDesc> _param)
        {
            if (_param == null) _param = new List<RNG.helpers.ParameterDesc>();
            param = _param;
        }
        public ReportBuildParams(string _name, string _bid, List<RNG.helpers.ParameterDesc> _param)
        {
            if (_param == null) _param = new List<RNG.helpers.ParameterDesc>();
            name = _name;
            bid = _bid;
            param = _param;
        }
    }
    // ---------------------------------------
    internal static DateTime bid_to_date(string bid)
    {
        return DateTime.ParseExact(bid + "01", "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
    }
    // ---------------------------------------
    private static RNG.Manager _instance;
    private static Oracle.DataAccess.Client.OracleConnection oc;
    private static mcpr4ead.console.Engine engine;
    // ---------------------------------------
    public static RNG.Manager Instance
    {
        get
        {
            if (_instance == null)
            {
                oc = ConnectionManager.Connection;
                oc.Open();
                engine = mcpr4ead.console_implements.CreateEngine();
                _instance = new RNG.Manager();

                Dictionary<string, string> cp = new Dictionary<string, string>();
                cp.Add("nostart", ConfigurationManager.AppSettings["report_connectParam"]);
                cp.Add("sip", ConfigurationManager.AppSettings["report_connectParamsip"]);
                
                RNG.helpers.Module.init(oc, cp,
                                            ConfigurationManager.AppSettings["report_execPath"],
                                            ConfigurationManager.AppSettings["report_ReportPath"],
                                            ConfigurationManager.AppSettings["report_GenPath"]);
            };
            return _instance;
        }
    }
    static RG_wlad() {
      Instance.ToString();
    }
    // ---------------------------------------
    private static Dictionary<int,List<RNG.helpers.ReportDesc>> _reportlists;
    public static List<RNG.helpers.ReportDesc>   getReportList(int id)
    {
        if(_reportlists==null)
            _reportlists= new Dictionary<int,List<RNG.helpers.ReportDesc>>();
        if(!_reportlists.ContainsKey(id))
        {
            var l=Instance.getReportList(id);
            if(l!=null)
            {
                _reportlists.Add(id,l);
                return l;
            }
        }else
            return _reportlists[id];
        return null;
    }
    // ---------------------------------------
    public static List<RNG.helpers.ReportDesc> ReportListCard
    {
        get
        {
            return getReportList(1);
        }
    }
    // ---------------------------------------
     public static void Reset()
    {
        _reportlists = null;
        _instance=null;
    }
    // ---------------------------------------
    public static List<SelectListItem> GetList(string sql)
    {
        var s = RNG.helpers.Select.Exec(sql);
        if (s != null)
        {
            var ret = new List<SelectListItem>();
            while (s.odr.Read())
            {
                var sli = new SelectListItem();
                sli.Value = s.odr.GetValue(0).ToString();
                sli.Text = s.odr.GetValue(1).ToString();
                ret.Add(sli);
            };
            s.Release();
            return ret;
        }
        return null;
    }
    // ---------------------------------------
    public static List<SelectListItem> GetListParamValues(RNG.helpers.ParameterDesc pd)
    {
        if (pd != null)
            if (pd.type == RNG.helpers.ParamType.List)
                return GetList(pd.arg);
        return null;
    }
    // ---------------------------------------
    internal static ActionResult Build_USER_BID_Report(ReportBuildParams model,decimal userid)
    {
        string err = "";
        if ((model != null)&&(model.param != null))
        {
            mcpr4ead.console.Engine eng = engine;
            RNG.Report rep=Instance.getReport(model.name,bid_to_date(model.bid));
            if(rep!=null)
            {
                List<RNG.helpers.ParameterDesc> prm = Instance.getParams(rep);
                Dictionary<string, object> args = new Dictionary<string, object>();
                Query q = SQL.GetSipIdByStartId;
                q[":user_id"].Value = userid;
                List<object> list = Database.Instance.Execute(q);
                foreach (var i in list) {
                    object[] objs = (object[]) i;
                    eng.Add_Variable("sip_user_id", objs[0].ToString(), "0", mcpr4ead.console.ItemAttr.Blank, null);
                };
                eng.Add_Variable("user_id", userid.ToString(), "0", mcpr4ead.console.ItemAttr.Blank, null);
                eng.Add_Variable("billing_id", model.bid, "201112", mcpr4ead.console.ItemAttr.Blank, null);

                args = RNG.helpers.helper.FillParams(eng,prm, model.param, args);

                RNG.Result result=rep.Build(args);
                if(result!=null)
                {
                    var ret = new System.Web.Mvc.FileStreamResult(result.getData(), result.GetContentType());
                    ret.FileDownloadName = result.getFileDownloadName();
                    return ret;
                };
                throw new Exception("Error: Report Result not resolved");
            }
                throw new Exception("Error: Report model not found");
        }
                throw new Exception("Error: input parameters expected");
    }
    // ---------------------------------------
    internal static ActionResult BuildReport(ReportBuildParams model)
    {
        string err = "";
        if ((model != null) && (model.param != null))
        {
            mcpr4ead.console.Engine eng = engine;
            RNG.Report rep = Instance.getReport(model.name, DateTime.Now);
            if (rep != null)
            {
                List<RNG.helpers.ParameterDesc> prm = Instance.getParams(rep);
                Dictionary<string, object> args = new Dictionary<string, object>();
                args = RNG.helpers.helper.FillParams(eng, prm, model.param, args);
                RNG.Result result = rep.Build(args);
                if (result != null)
                {
                    var ret = new System.Web.Mvc.FileStreamResult(result.getData(), result.GetContentType());
                    ret.FileDownloadName = result.getFileDownloadName();
                    return ret;
                };
                throw new Exception("Error: Report Result not resolved");
            }
            throw new Exception("Error: Report model not found");
        }
        throw new Exception("Error: input parameters expected");
    }
    // ---------------------------------------
    internal static ActionResult PrintRequest(string type,decimal Number)
    {
        if (Instance.getReport("req_"+type, DateTime.Now) != null)
        {
            ReportBuildParams rp =
            new ReportBuildParams("req_" + type, null, null);
            rp.param.Add(new RNG.helpers.ParameterDesc("num", RNG.helpers.ParamType.Number, "", "", "", Number.ToString()));
            return BuildReport(rp);
        }
        else
        {
            var c = new ContentResult();
            c.Content = "Wrong request type";
            return c;
        }
    }
}
// -------------------------------------------

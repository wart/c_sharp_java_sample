﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
// --------------------------------------------------------
namespace mcpr4ead
{
    // ----------------------------------------------------
    public class console_implements
    {
        internal class ALI : console.ArgumentListInterface
        {
            internal List<string> args;
            internal List<string> rawargs;
            // --------------------------------------------
            internal ALI()
            {
                args = new List<string>();
            }
            // --------------------------------------------
            public override int Count()
            {
                return args.Count;
            }
            // --------------------------------------------
            public override string String(int id)
            {
                if (args.Count > id)
                    return args[id];
                return "";
            }
            // --------------------------------------------
            public override float Float(int id)
            {
                try
                {
                    return float.Parse(String(id));
                }
                catch (Exception e) { };
                return 0;
            }
            // --------------------------------------------
            public override int Int(int id)
            {
                try
                {
                    return int.Parse(String(id));
                }
                catch (Exception e) { };
                return 0;
            }
            // --------------------------------------------
			public void		initarg			(string s)
			{
				if (s==null) return;
                int len=s.Length;
				if (len==0) return;
				int n=0;
                StringBuilder  ss= new StringBuilder();
                while ((n < len) && (s[n] == ' ')) n++;
                while(n<len)
				{
                    ss.Clear();
                    while ((n < len) && (s[n] != ' '))
					{
						if (s[n]=='"')
                        {
							n++;
                            while ((n < len)&&(s[n] != '"'))
							{
								if (s[n]=='\\') n++;
								if(n<len) ss.Append(s[n]);
								n++;
							};
						
                        }
                        else
                        {
							if (s[n]=='\\')
                            {
                                n++;
                                if(n<len)
							        ss.Append(s[n]);
                            }else
                                ss.Append(s[n]);
						};
						n++;
					};
					args.Add(ss.ToString().Trim());
                    while ((n < len) && (s[n] == ' ')) n++;
			    };
		    }
            // --------------------------------------------
            public void		initcmd			(string s)
			{
				if (s==null) return;
                int len=s.Length;
				if (len==0) return;
				int n=0;
                StringBuilder  ss= new StringBuilder();
                while ((n < len) && ((s[n] == ' ') || (s[n] == ';'))) n++;
				while (n<len)
				{
					ss.Clear();
                    while ((n < len) && (s[n] != ';'))
					{
						if (s[n]=='"')
						{
							ss.Append(s[n]);
							n++;
                            while ((n < len) && (s[n] != '"'))
							{
								if (s[n]=='\\')
								{
									ss.Append(s[n]);
									n++;
								};
								if(n<len)ss.Append(s[n]);
								n++;
							};
							if(n<len)ss.Append(s[n]);
							n++;
						};
                        if ((n < len) && (s[n] != ';'))
							ss.Append(s[n]);
						n++;
					};
                    if (ss.Length>0)
                        args.Add(ss.ToString().Trim());
                    while ((n < len) && (s[n] == ';')) n++;
				};
			}
            // --------------------------------------------
            public override string ToString()
            {
                return string.Join(" ",args.ToArray());
            }
            // --------------------------------------------
            public string DebugToString()
            {
                return string.Join(" , ", args.ToArray());
            }            
            // --------------------------------------------
            public override string ToString(int from)
            {
                string s="";
                for (int i = from; i < args.Count; ++i)
                    s += " " + args[i];
                return s;
            }
            // --------------------------------------------
            public override string RawToString(int from)
            {
                string s = "";
                if(this.rawargs!=null)
                    for (int i = from; i < rawargs.Count; ++i)
                        s += " " + rawargs[i];
                return s;
            }
            // --------------------------------------------
            public void done()
            {
                args.Clear();
            }
        };
        // ------------------------------------------------
        internal class Variable: console.Object
		{
            internal string _value;
            internal string _default;
            internal console.ItemAttr  _attrib;
            // --------------------------------------------
            public override console.ItemType GetType()
			{
				return console.ItemType.Var;
            }
            // --------------------------------------------
			public int fint()
			{
                try
                {
                    return int.Parse(_value);
                }
                catch (Exception e) { };
                return 0;
			}
            // --------------------------------------------
			public float fflt()
			{
                try
                {
                    return float.Parse(_value);
                }
                catch (Exception e) { };
                return 0;
			}
            // --------------------------------------------
			public string	fstr()
			{
                return _value;
			}
            // --------------------------------------------
			public bool		pstr(string	val)
			{
				if (can_change()) return false;
                _value=val;
				_attrib|=console.ItemAttr.Changed;
                return true;
			}
            // --------------------------------------------
            public bool pflt(float val)
			{
				if (can_change()) return false;
                _value=val.ToString();
                _attrib|=console.ItemAttr.Changed;
                return true;
			}
            // --------------------------------------------
            public bool pint(int val)
			{
				if (can_change()) return false;
                _value=val.ToString();
                _attrib|=console.ItemAttr.Changed;
                return true;
			}
            // --------------------------------------------
            public bool can_change()
			{
                if (_attrib.HasFlag(console.ItemAttr.Sys)) return false;
				return true;
			}
            // --------------------------------------------
        }
        // ------------------------------------------------
		internal class Procedure: console.Object
		{
            // --------------------------------------------
            public override console.ItemType GetType()
			{
				return console.ItemType.Proc;
            }
            // --------------------------------------------
            internal console.ProcedureEnterPoint _point;
			internal string				        _description;
            internal bool needprep;
            // --------------------------------------------
        }
        // ------------------------------------------------
		internal class Alias: console.Object
		{
            // --------------------------------------------
            internal string _value;
            // --------------------------------------------
            public override console.ItemType GetType()
			{
				return console.ItemType.Alias;
            }
            // --------------------------------------------
		};
        // ------------------------------------------------
	    internal class Folder: console.Object
		{
            internal Dictionary<string,console.Object> items;
            internal console.ItemAttr  _attrib;
            // --------------------------------------------
            public override console.ItemType GetType()
			{
				return console.ItemType.Folder;
            }
            // --------------------------------------------
            public Folder()
            {
                items= new Dictionary<string,console.Object>();
            }
            // --------------------------------------------
        };
        // ------------------------------------------------
        internal class Engine : console.Engine
        {
            internal Folder _root;
            internal console.OutputMethod outt;
            internal Stack<ALI> stack;
            // --------------------------------------------
            internal void output(string msg,params object[] args)
            {
                if(outt!=null)
                    outt(msg,args);
            }
            // --------------------------------------------
            public Engine()
            {
                _root=new Folder();
                stack = new Stack<ALI>();
                Add_Procedure("def"  , def, "");
                Add_Procedure("alias", def, "");
                Add_Procedure("set"  , _set, "");
                Add_Procedure("undef", _delete, "");
                Add_Procedure("unset", _delete, "");
                Add_Procedure("isset", _isdef, "");
                Add_Procedure("isdef", _isdef, "");
                Add_Procedure("mkc", def_folder, "");
                Add_Procedure("if_eq", def_folder, "");
                
            }
            // --------------------------------------------
            internal void pop()
            {
                if (stack.Count > 0)
                    stack.Pop();
            }
            // --------------------------------------------
            internal string getArg( int id)
            {
                if (stack.Count > 0)
                    return stack.Peek().String(id);
                return "0";
            }
            // --------------------------------------------
            internal void push(ALI a)
            {
                stack.Push(a);
            }
            // --------------------------------------------
            internal bool isDeletable(console.Object o)
			{
				if (o!=null)
					switch (o.GetType())
					{
						case console.ItemType.Var:
							return     (((Variable)o)._attrib.HasFlag(console.ItemAttr.Sys))?false:true;
                        break;
					    case console.ItemType.Alias:
						    return true;
                        break;
                        default:return true;
                    };
				return true;
			}
            // --------------------------------------------
			internal Folder	getRoot(console.Object fld)
			{
				if((fld!=null)&&(fld.GetType()==console.ItemType.Folder))
                   return (Folder)fld;
                return this._root;
			}
            // --------------------------------------------
            internal console.Object def(console.ArgumentListInterface args)
            {
                if (args.Count() >= 3)
                {
                    console.Object parent,item;
                    string ident;
                    if(getByPath(args.String(1),out parent,out item,out ident,null))
                    {
                        console.Object co = Add_Alias(ident, args.RawToString(2), parent);
                        if (co != null)
                        {
                            return CreateResult(1);
                        };
                    };
                };
                return CreateResult(0);
            }
            // --------------------------------------------
            internal console.Object def_folder(console.ArgumentListInterface args)
            {
                if (args.Count() >= 2)
                {
                    console.Object parent, item;
                    string ident;
                    if (getByPath(args.String(1), out parent, out item, out ident, null))
                    {
                        console.Object co = Add_Folder(ident,console.ItemAttr.Blank,parent);
                        if (co != null)
                        {
                            return CreateResult(1);
                        };
                    };
                };
                return CreateResult(0);
            }
            // --------------------------------------------
            internal console.Object _set(console.ArgumentListInterface args)
            {
                if (args.Count() >= 3)
                {
                    console.Object parent, item;
                    string ident;
                    if (getByPath(args.String(1), out parent, out item, out ident, null))
                    {
                        if ((item != null)&&(setValueString(item, args.String(2))))
                           return CreateResult(1);
                        else
                            if ((Add_Variable(ident, args.ToString(2), null, console.ItemAttr.Blank, parent)) != null)
                                return CreateResult(1);
                    };
                };
                return CreateResult(0);
            }
            // --------------------------------------------
            internal console.Object _delete(console.ArgumentListInterface args)
            {
                if (args.Count() >= 2)
                {
                    console.Object parent, item;
                    string ident;
                    if (getByPath(args.String(1), out parent, out item, out ident, null))
                    {
                        if ((DelItem(ident, parent))!=null)
                            return CreateResult(1);
                    }
                }
                return CreateResult(0);
            }
            // --------------------------------------------
            internal console.Object _isdef(console.ArgumentListInterface args)
            {
                if (args.Count() >= 2)
                {
                    if ((getByPath(args.String(1)))!=null)
                        return CreateResult(1);
                }
                return CreateResult(0);
            }
            // --------------------------------------------
            internal console.Object if_eq(console.ArgumentListInterface args)
            {
               if (args.Count() >= 4)
                {
                   if(args.String(1).Equals(args.String(2)))
                       return CreateResult(args.String(3));
                   if (args.Count()>=5)
                   return CreateResult(args.String(4));
               };
                return null;
            }/**/
            // --------------------------------------------
			private bool	Add(string name,console.Object itm,console.Object dst = null)
			{
                if((itm!=null)&&(name!=null)&&(name.Length>0))
                {
                    Folder f=getRoot(dst);
				    if(f!=null)
                    {
                        
                        if(f.items.ContainsKey(name))
                        {
                            if (isDeletable(f.items[name]))
                                f.items[name] = itm;
                            else
                                return false;
                        }
                        else
                        {
                            f.items.Add(name,itm);
                        };
                        return true;
                    }
                };
				return false;
			}
            // --------------------------------------------
			console.Object get(string name,console.Object src=null)
			{
                Folder f=getRoot(src);
				if(f!=null)
                    if(f.items.ContainsKey(name))
                        return f.items[name];
                return null;
			}
            // --------------------------------------------
            internal string checkName(string nam)
            {
                if (nam != null)
                {
                    nam = nam.Trim();
                    string n=Regex.Replace(nam, @"[a-z,A-Z,_]", string.Empty);
                    if (n.Equals(""))
                        return nam;
                };
                return null;
            }
            // --------------------------------------------
            public override console.Object Add_Procedure(string name, console.ProcedureEnterPoint pep, string descr, bool needprepargs = true, console.Object dst = null)
            {
                if(pep!=null)
                    if((name=checkName(name))!=null)
                {

                    Procedure p= new Procedure();
                    p._point=pep;
                    p.needprep = needprepargs;
                    p._description=descr;
                    if(Add(name,p,dst))
                        return p;
                };
                return null;
            }
            // --------------------------------------------
            public override console.Object CreateResult(string ret)
            {
                Variable v = new Variable();
                v._value = ret;
                return v;
            }
            // --------------------------------------------
            public override console.Object CreateResult(int ret)
            {
                Variable v = new Variable();
                v._value = ret.ToString();
                return v;
            }
            // --------------------------------------------
            public override console.Object CreateResult(float ret)
            {
                Variable v = new Variable();
                v._value = ret.ToString();
                return v;
            }
            // --------------------------------------------
            public override console.Object Add_Variable(string name, string val, string def_val, console.ItemAttr a, console.Object dst = null)
            {
                if ((name = checkName(name)) != null)
                {
                    Variable v = new Variable();
                    v._attrib = a;
                    v._value = val;
                    v._default = def_val;
                    if (Add(name, v, dst))
                        return v;
                };
                return null;
            }
            // --------------------------------------------
            public override console.Object Add_Folder(string name, console.ItemAttr a, console.Object dst = null)
            {
                if ((name = checkName(name)) != null)
                {
                    Folder f = new Folder();
                    f._attrib = a;
                    if (Add(name, f, dst))
                        return f;
                }
                return null;
            }
            // --------------------------------------------
            public override console.Object Add_Alias(string name, string val, console.Object dst = null)
            {
                if ((name = checkName(name)) != null)
                {
                    Alias a = new Alias();
                    a._value = val;
                    if (Add(name, a, dst))
                        return a;
                }
                return null;
            }
            // --------------------------------------------
            public override console.Object getProcedure(string name, console.Object src = null)
            {
                Procedure p=(Procedure)get(name,src);
                if((p!=null)&&(p.GetType()==console.ItemType.Proc))
                    return p;
                return null;
            }
            // --------------------------------------------
            public override console.Object getVariable(string name, console.Object src = null)
            {
                Variable v=(Variable)get(name,src);
                if((v!=null)&&(v.GetType()==console.ItemType.Var))
                    return v;
                return null;
            }
            // --------------------------------------------
            public override console.Object getAlias(string name, console.Object src = null)
            {
                Alias a=(Alias)get(name,src);
                if((a!=null)&&(a.GetType()==console.ItemType.Alias))
                    return a;
                return null;
            }
            // --------------------------------------------
            public override console.Object getFolder(string name, console.Object src = null)
            {
                Folder f=(Folder)get(name,src);
                if((f!=null)&&(f.GetType()==console.ItemType.Folder))
                    return f;
                return null;
            }
            // --------------------------------------------
            public override console.Object getByPath(string path, console.Object src = null)
            {
                if (path != null)
                {
                    string[] sa = path.Split('.');
                    if ((sa != null)&&(sa.Length>0))
                    {
                        
                        foreach (var nm in sa)
                        {
                            src = get(nm, src);
                            if (src == null)
                                return null;
                        };
                        return src;
                    }
                }
                return null;
            }
            // --------------------------------------------
            public override bool getByPath(string path, out console.Object parent, out console.Object item,out string ident, console.Object src = null)
            {
                parent = null; item = null; ident = null;
                if (path != null)
                {
                    string[] sa = path.Split('.');
                    if ((sa != null) && (sa.Length > 0))
                    {
                        int maxi = sa.Length - 1;
                        parent = getRoot(src);
                        item = null;
                        //sa[9] = "";
                        for (int i = 0; i <= maxi; ++i)
                        {
                            if (parent == null) break;
                            item = get(sa[i], parent);
                            if (i < maxi)
                                parent = item;
                        };
                        if(parent!=null)
                        {
                            ident=sa[maxi];
                            return true;
                        }
                    }
                };
                return false;
            }
            // --------------------------------------------
            public override console.Object DelItem(string name, console.Object src = null)
            {
                Folder f=getRoot(src);
                if((f!=null)&&(f.items.ContainsKey(name)))
                {
                    console.Object co= f.items[name];
                    if (isDeletable(co))
                    {
                        f.items.Remove(name);
                        return co;
                    }
                }
                return null;
            }
            // --------------------------------------------
			string	ident_conv_var	(string	instr,int subloop)
			{
				StringBuilder outstr= new StringBuilder();
				StringBuilder _var= new StringBuilder();
				StringBuilder str = new StringBuilder();
				int l=instr.Length;
				int n=0;
				while (n<l)
				{
                    _var.Clear();
                    str.Clear();
                    while ((n < l) && (instr[n] != '$'))
					{
						if (instr[n]=='\\')
							n++;
						str.Append(instr[n++]);
					};
					if ((n<l-2)&&(instr[n]=='$')&&(instr[n+1]=='{'))
					{
						n+=2;
                        while ((n < l) && (instr[n] != '}'))
						{
							if (instr[n]=='\\')
								n++;
							 if (n < l)_var.Append(instr[n++]);
						};
                        if ((n < l) && (instr[n] != '}'))
                            _var.Clear();
					}
					else
                        if (n < l) str.Append(instr[n]);
					outstr.Append(str);
                    if (_var.Length > 0)
                    {
                        int id=-1;
                        string dd=_var.ToString();
                        try
                        {
                            id=int.Parse(dd);
                        }catch(Exception e){};
                        if(id<0)
                        {
                            var o = call_alias(dd, null, subloop + 1);
                            if (o != null)
                                outstr.Append(getValueString(o));
                        }else
                            outstr.Append(getArg(id));
                    };
					n++;
				};
				return outstr.ToString();
			}
            // --------------------------------------------
			private console.Object	call_var(Variable v,ALI Args)
			{
				if(v!=null)
				{
					if (Args.Count()>=2)
					{
                        v.pstr(Args.String(1));
					}
					else
					{
//						output("{0} = {1}  default: {2}",Args.String(0),  v._value, v._default);
					};
				};
                return v;
			}
            // --------------------------------------------
            private ALI prepare_args(ALI Args,bool np, int subloop)
            {
   //             output("prepare_args");
                if(np)
                {
                    Args.rawargs = new List<string>();
                    for (int i = 0; i < Args.args.Count; ++i)
                    {
                        Args.rawargs.Add(Args.args[i]);
                        Args.args[i] = ident_conv_var(Args.args[i], subloop + 1);
                    }
                }else
                    Args.rawargs=Args.args;
                return Args;
            }
            // --------------------------------------------
			private void	Dispatch_cmd(ALI Args,int subloop)
			{
			    output("unknown command: {0}",Args.ToString());
			}
            // --------------------------------------------
            private console.Object	call_cmd	(ALI Args,int subloop)
			{
				if (Args.Count()==0) return null;

				if (console.max_loop_depth<=subloop)
				{
					output("Maximal Sub Iteration Depth of cmd looping. command ({0})ignored",Args.String(0));
					return null;
				};
                console.Object r = getByPath(ident_conv_var(Args.String(0), subloop), null);
				if (r!=null)
				{
					switch (r.GetType())
					{
						case console.ItemType.Var  :
                            Args = prepare_args(Args,true, subloop);
                            return call_var((Variable)r,Args);break;
                        case console.ItemType.Alias:
                            Args = prepare_args(Args, true, subloop);
                            return call_alias(((Alias)r)._value, Args,subloop + 1); break;
						case console.ItemType.Proc :
								{
                                    Args = prepare_args(Args, ((Procedure)r).needprep, subloop);
                                    if(((Procedure)r)._point!=null)
                                        return ((Procedure)r)._point(Args);
								};break;
					};
				}else{Dispatch_cmd(Args,subloop+1);};
                return null;
			}
            // --------------------------------------------
            private console.Object	call_alias	(string	  cmsd,ALI Args, int subloop)
			{
                console.Object ret=null,rr=null;
				ALI cmdss=new ALI();
				ALI argss=new ALI();
				cmdss.initcmd(cmsd.Trim());
				if (cmdss.Count()==0) return null;
                if (Args!=null) push(Args);
               // output("alias body {0}", cmdss.ToString());

				for(int j=0;j<cmdss.Count();j++)
				{
					//output("alias {0}=={1}",j,cmdss.String(j));
                    
					argss.initarg(cmdss.String(j));
                   // output("alias args {0}", j, argss.ToString());
					rr=call_cmd(argss,subloop);
                    if(rr!=null)ret=rr;
					argss.done();
				};
                if (Args != null) pop();
                return ret;
			}
            // --------------------------------------------
            public override console.Object Exec(string cmds)
            {
               // output(cmds);
				if ((cmds.Length>0)&&(cmds!=null))
                {              
                    cmds=cmds.Replace("\n",";");
                    cmds=cmds.Replace("\r",";");
          //          output("Batch executed..");
					return call_alias(cmds,null,1);
                    
				};
                return null;
            }
            // --------------------------------------------
            public override string ExecS(string cmds)
            {
               // output(cmds);
                var o=Exec(cmds);
                if(o!=null)
                    return getValueString(o);
                return "";
            }
            // --------------------------------------------
            public override string getValueString(console.Object src)
            {
                if ((src != null) && (src.GetType() == console.ItemType.Var))
                {
                    return ((Variable)src).fstr();
                }
                return "";
            }
            // --------------------------------------------
            public override float getValueFloat(console.Object src)
            {
                if ((src != null) && (src.GetType() == console.ItemType.Var))
                {
                    return ((Variable)src).fflt();
                }
                return 0;
            }
            // --------------------------------------------
            public override int getValueInt(console.Object src)
            {
                if ((src != null) && (src.GetType() == console.ItemType.Var))
                {
                    return ((Variable)src).fint();
                }
                return 0;
            }
            // --------------------------------------------
            public override bool setValueString(console.Object dst, string src)
            {
                if ((dst != null) && (dst.GetType() == console.ItemType.Var))
                {
                    return ((Variable)dst).pstr(src);
                }
                return false;
            }
            // --------------------------------------------
            public override bool setValueFloat(console.Object dst, float src)
            {
                if ((dst != null) && (dst.GetType() == console.ItemType.Var))
                {
                    return ((Variable)dst).pflt(src);
                }
                return false;
            }
            // --------------------------------------------
            public override bool setValueInt(console.Object dst, int src)
            {
                if ((dst != null) && (dst.GetType() == console.ItemType.Var))
                {
                    return ((Variable)dst).pint(src);
                }
                return false;
            }
            // --------------------------------------------
            public override void addOutput(console.OutputMethod m)
            {
                outt+=m;
            }
            // --------------------------------------------
        };
        // ------------------------------------------------
        public static console.Engine CreateEngine()
        {
             return new console_implements.Engine();
        }
        // ------------------------------------------------
    }
    // ----------------------------------------------------
}
// --------------------------------------------------------

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mcpr4ead
{
    public class console
    {
        // --------------------------------------------
        public enum ItemType
        {
        	Var		= 0x01,
		    Proc	= 0x02,
		    Alias   = 0x03,
		    Folder	= 0x04
        }
        // --------------------------------------------
        [Flags]
        public enum ItemAttr
        {
            Blank     = 0x00,
		    Sys		  = 0x01,
		    Keep	  = 0x02,
		    Readonly  = 0x04,
		    Changed	  = 0x08
        }
        // --------------------------------------------
        public const int max_loop_depth = 0x40;
        public const int max_Stack_depth = 0x40;
        // --------------------------------------------


        // --------------------------------------------
        public abstract class ArgumentListInterface
		{
            public abstract int     Count();
            public abstract string  String(int id);
            public abstract float   Float(int id);
            public abstract int     Int(int id);
            public abstract string ToString(int from);
            public abstract string RawToString(int from);
		};
        // --------------------------------------------
		public abstract class Object
		{
			 public abstract ItemType GetType();
        }
        // --------------------------------------------
        public delegate Object ProcedureEnterPoint(ArgumentListInterface args);
        public delegate void OutputMethod(string msg,params object[] args);
        // --------------------------------------------
		public abstract class Engine
		{
            public abstract Object Add_Procedure(string name, ProcedureEnterPoint pep, string descr, bool needprepargs = true, Object dst = null);
            public abstract Object Add_Variable(string name, string val, string def_val, ItemAttr a, Object dst = null);
            public abstract Object Add_Folder(string name, ItemAttr a, Object dst = null);
            public abstract Object Add_Alias(string name, string val, Object dst = null);

            public abstract Object getProcedure(string name, Object src = null);
            public abstract Object getVariable(string name, Object src = null);
            public abstract Object getAlias(string name, Object src = null);
            public abstract Object getFolder(string name, Object src = null);

            public abstract Object getByPath(string path, Object src = null);
            public abstract bool   getByPath(string path, out Object parent, out Object item, out string ident, Object src = null);
            public abstract Object DelItem(string name, Object src = null);
            public abstract Object Exec(string cmds);
            public abstract string ExecS(string cmds);
            public abstract void   addOutput(OutputMethod m);

            public abstract string getValueString(Object src);
            public abstract float getValueFloat(Object src);
            public abstract int getValueInt(Object src);
            public abstract bool setValueString(Object dst, string src);
            public abstract bool setValueFloat(Object dst, float src);
            public abstract bool setValueInt(Object dst, int src);
            public abstract Object CreateResult(string ret);
            public abstract Object CreateResult(int ret);
            public abstract Object CreateResult(float ret);
		};

        // --------------------------------------------
    }
}

package Tamplate;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

public class Builder
{
	private Vector<Iteration>		stack;
	private StringBuffer			sb;
	private TreeMap<String,Object>	params;
	private Connection				conn;
	private Node					ROOT;
	//----------------------------------------------------------
	private Builder(TreeMap<String,Object> in,StringBuffer sbb,Connection con)
	{
		sb				=	(sbb==null)?new StringBuffer():sbb;
		stack			=	new Vector<Iteration>();
		params			=	new TreeMap<String,Object>();
		conn			=	con;
		tool.CopyTreeMap(in,params,"IN.");
	}
	//----------------------------------------------------------
	private void pop()
	{
		if(stack.size()>0)stack.remove(stack.size()-1);
	};	
	//----------------------------------------------------------
	private void push(Iteration it) throws Exception
	{
		if(stack.size()<1024)
		{
			if(it!=null)
			stack.add(it);
		}else throw new Exception("Stack overflow");
	}
	//----------------------------------------------------------
	private Node GetNamedBlock(String nam)
	{
		return (ROOT!=null)?ROOT.RootGetNamedBlock(nam):null;
	};
	//----------------------------------------------------------
	private ResultSet init_request(String sql,Vector<String> n) throws Exception
	{
		return tool.init_request(sql, n, params, conn);
	};
	//----------------------------------------------------------
	private boolean PrepareFields(ResultSet rs,String pref) throws Exception
	{
		//if()
		return tool.PrepareFields(rs,pref+".",params);
	}
	//----------------------------------------------------------
	private void prepare_Node(Node n) throws Exception
	{
		if(n!=null)
		{
			switch(n.Class)
			{
				case	Node.ID_NODE_ROOT	:push(Iteration.it_Contaneer(n.childs0));break;
				case	Node.ID_NODE_PRN	:Object o=params.get(n.name);if(o!=null)sb.append(o);break;
				case	Node.ID_NODE_SINGLE	:PrepareFields(init_request(n.Value,n.param),n.name);break;
				
				case	Node.ID_NODE_EXEC	:ExecEXEC(n);break;
				case	Node.ID_NODE_REFC	:ExecREFC(n);break;
				
				case	Node.ID_NODE_FETCH	:push(Iteration.it_Fetch(n,init_request(n.Value,n.param)));break;
				case	Node.ID_NODE_STATIC	:sb.append(n.Value);break;
				case	Node.ID_NODE_BLOCK	:if(n.allowBlockBuildInPlace())push(Iteration.it_Contaneer(n.childs0));break;
				case	Node.ID_NODE_PUTBLOCK:{Node nn=GetNamedBlock(n.name);if(nn!=null) push(Iteration.it_Contaneer(nn.childs0));};break;
				case	Node.ID_NODE_SSET	:params.put(n.name,n.Value);break;
				case	Node.ID_NODE_IF		:break;				
				default	: throw new Exception("Unknown Node Class found ("+n.Class+")");
			};
		};
	}
	//----------------------------------------------------------
	private void ExecEXEC(Node n) throws Exception
	{
		tool.ExecEXEC(n.Value,n.param,params,conn);
	}
	//----------------------------------------------------------
	private void ExecREFC(Node n) throws Exception
	{
		tool.ExecREFC(n.Value,n.param,params,conn);
	}	
	//----------------------------------------------------------
	private void HandleCONTANEER(Iteration it) throws Exception
	{
		if(it.childs.size()>it.step)
		{
			prepare_Node(it.childs.get((int)it.step));
			it.step++;
		}else pop();
	}
	//----------------------------------------------------------
	private void HandleFETCH(Iteration it) throws Exception
	{
		if(PrepareFields(it.rs,it.src.name))
		{
			
			it.step++;
			push(Iteration.it_Contaneer(((((int)(it.step / 2))*2)==it.step)?((it.src.childs1!=null)?it.src.childs1:it.src.childs0):it.src.childs0));
			if((it.step==1)&&(it.src.childs3!=null))
				push(Iteration.it_Contaneer(it.src.childs3));
		}else
		{
			pop();
			if ((it.step==0)&&(it.src.childs2!=null))
				push(Iteration.it_Contaneer(it.src.childs2));
		}
	}
	//----------------------------------------------------------
	private void HandleIteration(Iteration it) throws Exception
	{
		if(it!=null)
		{
			switch(it.Class)
			{
				case	Iteration.ID_CONTANEER:HandleCONTANEER	(it);break;
				case	Iteration.ID_FETCH	  :HandleFETCH		(it);break;
				default: throw new Exception("unknown it.Class: "+it.Class); 
			}
		}else pop();
	}
	//----------------------------------------------------------
	private StringBuffer runInner(Node n) throws Exception
	{
		if (n.Class!=Node.ID_NODE_ROOT) throw new Exception("Node  must be is ROOT Class");
		ROOT=n;
		prepare_Node(n);
		int sz= stack.size();
		while(sz>0)
		{
			HandleIteration(stack.get(sz-1));
			sz= stack.size();
		};
		return sb;
	}
	//----------------------------------------------------------	
	static public StringBuffer run(Node n,TreeMap<String,Object> IN,StringBuffer sbb,Connection conn) throws Exception
	{
		if (n!=null)
		{
			Builder b= new Builder(IN,sbb,conn);
			return b.runInner(n); 
		};
		return null;
	}
	//----------------------------------------------------------
	static public String run(String templ,TreeMap<String,Object> IN,Connection conn) throws Exception
	{
		   StringBuffer sb = new StringBuffer();		   
		   Parcer c= new Parcer();
		   Node n=c.ParceTemplate(templ);
		   if(n!=null)
		   {
			   //Node.Debug_prn(sb,n,true);
			   if(conn!=null)
			   {
				   Builder.run(n,IN,sb,conn);
			   }
			   else
				   throw new Exception("connection is null");
		   }else throw new Exception("Node  is null");		
		return sb.toString();
	}
	
	//----------------------------------------------------------	
}

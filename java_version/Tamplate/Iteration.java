package Tamplate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.TreeMap;
import java.util.Vector;

public class Iteration
{
	public final static int		ID_CONTANEER	=0;
	public final static int		ID_FETCH   		=1;
	//-----------------------------------------------------------------------
	public int 			Class;

	Vector<Node>		childs;	
	public long			step;
	
	public ResultSet	rs;
	public Node			src;
	//-----------------------------------------------------------------------
	private Iteration(int cl){Class=cl;};
	//-----------------------------------------------------------------------
	static public Iteration it_Fetch(Node n,ResultSet r)
	{
		Iteration it=null;
		if((n!=null)&&(r!=null))
		{
			it= new Iteration(ID_FETCH);
			it.rs=r;
			it.step=0;
			it.src=n;
		};		
		return it;
	}
	//-----------------------------------------------------------------------
	static public Iteration it_Contaneer(Vector<Node> chl)
	{
		Iteration it=null;
		if(chl!=null)
			if(chl.size()>0)
			{
				it= new Iteration(ID_CONTANEER);
				it.childs=chl;
				it.step=0;
			};
		return it;
	}
	//-----------------------------------------------------------------------
}
	
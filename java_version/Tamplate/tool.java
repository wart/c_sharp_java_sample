package Tamplate;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
//import java.sql.ParameterMetaData;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import oracle.jdbc.OracleTypes;
import oracle.jdbc.driver.OracleCallableStatement;
import oracle.jdbc.driver.OracleParameterMetaData;

public class tool {

	public static void CopyTreeMap(TreeMap<String,Object> src,TreeMap<String,Object>dst,String pref)
	{
		Set<String> s =	null;
		
		if((src!=null)&&(dst!=null))
			if((s=src.keySet())!=null)
			{
				Object[] o=s.toArray();
				int sz=o.length;
				String sn;
				for (int i=0;i<sz;i++)
					if((sn=(String)o[i])!=null)
						dst.put(pref+sn,src.get(sn));
			}
	}
	//--------------------------------------------------------
	public static  boolean PrepareFields(ResultSet rs,String pref,TreeMap<String,Object>dst) throws Exception
	{
		if(rs!=null)
		{
			if(rs.next())
			{
				ResultSetMetaData md=	rs.getMetaData();
				int cnt=md.getColumnCount();
				for (int i=1;i<=cnt;i++)
				{
					Object o=null;
					int ct=md.getColumnType(i);
					switch(ct)
					{
						case java.sql.Types.BIGINT :o=rs.getLong   (i);break;
						case java.sql.Types.NUMERIC:
							o=rs.getDouble(i);
//							o=(md.isCurrency(i))?rs.getFloat(i):rs.getLong(i);
//							o=(md.getScale(i)==0)?rs.getFloat(i):rs.getLong(i);
							
							break;
						case java.sql.Types.VARCHAR:o=rs.getString (i);break;
						case java.sql.Types.BOOLEAN:o=rs.getBoolean(i);break;
					};
					dst.put(pref+md.getColumnLabel(i),o);
				};
				return true;
			};
		}
		return false;
	}	
	//----------------------------------------------------------
	public static ResultSet init_request(String sql,Vector<String> n,TreeMap<String,Object> params,Connection conn) throws Exception
	{
		ResultSet rs=null;
		if(conn!=null)
		{
			PreparedStatement ps = conn.prepareStatement(sql);
			if((n!=null)&&(params!=null))
			{
				String prm;
				Object pval=null;
				int sz=n.size();
				for (int i=0;i<sz;i++)
					if ((prm=n.get(i))!=null)
					{
						pval=params.get(prm);
						if(pval!=null)
						{
							String cn= pval.getClass().getSimpleName();			
							if (cn.equalsIgnoreCase("String" )){ps.setString(i+1, (String) pval);};
							if (cn.equalsIgnoreCase("Integer")){ps.setInt   (i+1, (Integer)pval);};
							if (cn.equalsIgnoreCase("Double" )){ps.setDouble(i+1, (Double) pval);};
							if (cn.equalsIgnoreCase("Long"   )){ps.setLong  (i+1, (Long)   pval);};
							if (cn.equalsIgnoreCase("Float"  )){ps.setFloat (i+1, (Float)  pval);};
							if (cn.equalsIgnoreCase("Date"   )){ps.setDate  (i+1, (Date)   pval);};			
						}else
							ps.setNull(i+1,java.sql.Types.NULL);
					}
				}
			rs = ps.executeQuery();
		};
		return rs;
	}
	//----------------------------------------------------------
	private static void Setprm(int num, Object pval,CallableStatement st) throws SQLException
	{
		if(pval!=null)
		{
			String cn= pval.getClass().getSimpleName();			
			if (cn.equalsIgnoreCase("String" )){st.setString(num, (String) pval);};
			if (cn.equalsIgnoreCase("Integer")){st.setInt   (num, (Integer)pval);};
			if (cn.equalsIgnoreCase("Double" )){st.setDouble(num, (Double) pval);};
			if (cn.equalsIgnoreCase("Long"   )){st.setLong  (num, (Long)   pval);};
			if (cn.equalsIgnoreCase("Float"  )){st.setFloat (num, (Float)  pval);};
			if (cn.equalsIgnoreCase("Date"   )){st.setDate  (num, (Date)   pval);};			
		}else
			st.setNull(num+1,java.sql.Types.NULL);
	}
	//----------------------------------------------------------
	public static void ExecEXEC(String sql,Vector<String> n,TreeMap<String,Object> params,Connection conn) throws Exception
	{
		OracleCallableStatement st=(OracleCallableStatement)conn.prepareCall("begin  "+sql+";end;");
		//CallableStatement st=conn.prepareCall("begin  "+sql+";end;");
		OracleParameterMetaData dd= (OracleParameterMetaData)st.getParameterMetaData();
		int cnt=dd.getParameterCount();
		if(n!=null)
		if(n.size()>=cnt)
			for(int num=1;num<=cnt;num++)
			{
				switch(dd.getParameterMode(num))
				{
					case ParameterMetaData.parameterModeOut:
						st.registerOutParameter(num, dd.getParameterType(num));
					break;
					case ParameterMetaData.parameterModeInOut:
					{
						String prm;
						if ((prm=n.get(num-1))!=null)
							Setprm(num,params.get(prm),st);
						st.registerOutParameter(num, dd.getParameterType(num));
					};
					break;						
					case ParameterMetaData.parameterModeIn:
					{
						String prm;
						if ((prm=n.get(num-1))!=null)
							Setprm(num,params.get(prm),st);
					};
					break;
					default:;
				};
			};
		st.executeQuery();
		
		for(int num=1;num<=cnt;num++)
		{
			String prm;
			if ((prm=n.get(num-1))!=null)
				switch(dd.getParameterMode(num))
				{
					case ParameterMetaData.parameterModeInOut:
					case ParameterMetaData.parameterModeIn:
						Object o=null;
						switch(dd.getParameterType(num))
						{
							case java.sql.Types.BIGINT :o=st.getLong   (num);break;
							case java.sql.Types.NUMERIC:o=st.getLong   (num);break;
							case java.sql.Types.VARCHAR:o=st.getString (num);break;
							case java.sql.Types.BOOLEAN:o=st.getBoolean(num);break;
						};
						params.put(prm,o);
					break;
				};
		}
	}
	//----------------------------------------------------------
	public static void ExecREFC(String sql,Vector<String> param, TreeMap<String, Object> params,
			Connection conn) throws SQLException
	{
		ResultSet rs=null;
		if(conn!=null)
		{
			CallableStatement ps = conn.prepareCall("begin  ?:="+sql+";end;");
			ps.registerOutParameter(1, OracleTypes.CURSOR);			
			if(param!=null)
			{
				String prm;
				Object pval=null;
				int sz=param.size();
				for (int i=0;i<sz;i++)
					if ((prm=param.get(i))!=null)
					{
						pval=params.get(prm);
						if(pval!=null)
						{
							String cn= pval.getClass().getSimpleName();			
							if (cn.equalsIgnoreCase("String" )){ps.setString(i+2, (String) pval);};
							if (cn.equalsIgnoreCase("Integer")){ps.setInt   (i+2, (Integer)pval);};
							if (cn.equalsIgnoreCase("Double" )){ps.setDouble(i+2, (Double) pval);};
							if (cn.equalsIgnoreCase("Long"   )){ps.setLong  (i+2, (Long)   pval);};
							if (cn.equalsIgnoreCase("Float"  )){ps.setFloat (i+2, (Float)  pval);};
							if (cn.equalsIgnoreCase("Date"   )){ps.setDate  (i+2, (Date)   pval);};			
						}else
							ps.setNull(i+2,java.sql.Types.NULL);
					}
			}
			ps.execute(); 
			rs = (ResultSet) ps.getObject(1);
			if(rs!=null)
				while (rs.next())
				{
					params.put(rs.getString(1),rs.getString(2));
					//System.out.println(rs.getString(0)); 
				} 
		};
	}
	//------------------------------------------------------------------
}

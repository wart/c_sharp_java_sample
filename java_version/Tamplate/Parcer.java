package Tamplate;

import java.util.Vector;


final public class  Parcer
{
	private	int cor_pos;
	private	int mode;
	private	int rettype;
	private Vector<Node> stack;
	public final static String tag_Begin   ="<%";
	public final static String tag_inSplit =":";
	public final static String tag_End	   ="%>";
	
	public  static boolean logging=true; 

	//=====================================================
	public Parcer()
	{
		start_parce();
	}
	//=====================================================
	private void start_parce()
	{
		cor_pos=-1;
		mode=0;
		rettype=0;
		stack= new Vector<Node>();
		stack.add(Node.NodeRoot());
	};
	//=====================================================
	private boolean pushContaneer(Node n)
	{
		if(n!=null)
			if(pushChild(n))
			{
				stack.add(n);
				return true;
			};
		return false;
	};
	//=====================================================
	private boolean pushChild(Node n)
	{
		if(n!=null)
		{
			Node p=stack.get(stack.size()-1);
			if(p!=null)
			{
				p.AddNode(n);
				return true;
			};
		};
		return false;
	};
	//=====================================================
	private boolean stepUp()
	{
		int sz=stack.size();
		if(sz>1)
		{
			stack.remove(sz-1);
			return true;
		}else
			return false;
	}
	//=====================================================
	private boolean stepSwitch(int md)
	{
		
		if (stack!=null)
			if(stack.size()>0)
			{
				Node n=stack.get(stack.size()-1);
				if(n!=null)
					return n.allowAndSwitchCh2(md);
			};
		return false;
	};
	//=====================================================
	private String get(String src)
	{
		if(src==null) return null;
		int len=src.length();
		if(len<=cor_pos) return null;
		boolean repeat=true;
		while(repeat)
		{
					
			repeat=false;
			switch(mode)
			{
				case 0:
				{
					int i=(cor_pos<0)?src.indexOf(tag_Begin):src.indexOf(tag_Begin, cor_pos);
					cor_pos=(cor_pos<0)?0:cor_pos;
					if(i>=0)
					{
						if (cor_pos!=i)
						{
							String ret=src.substring(cor_pos,i);
							cor_pos=i+tag_Begin.length();
							mode=1;
							rettype=0;
							return ret;
						}else
						{
							cor_pos=i+tag_Begin.length();
							mode=1;
							rettype=0;
							repeat=true;
						}
					}else
					{
						String ret=src.substring(cor_pos);
						cor_pos=len;
						rettype=0;
						return ret;
					}
				}
				case 1:
				{
					int i=(cor_pos<0)?src.indexOf(tag_End):src.indexOf(tag_End, cor_pos);
					cor_pos=(cor_pos<0)?0:cor_pos;
					if(i>=0)
					{
						if (cor_pos!=i)
						{
							String ret=src.substring(cor_pos,i);
							cor_pos=i+tag_End.length();
							mode=0;
							rettype=1;
							return ret;
						}else
						{
							cor_pos=i+tag_End.length();
							mode=0;rettype=1;
							repeat=true;
						}
					}else
					{
						cor_pos=len;
						rettype=2;
						return null;
					}
				}				
			};
		};
		return null;
 	}
	//=====================================================
	
	private void debug_strings(String[] s)
	{
		if(s!=null)
		{
			int sz=s.length;
			for(int i =0;i<sz;i++)
				System.out.print(i+": '"+s[i]+"'\n");
		}else System.out.print("String[] s=null");
		System.out.print("\n");
	};
	//=====================================================
	private boolean pushNode(String t)
	{
		if (t!=null)
		{
				String[] s=t.split(tag_inSplit);
				//debug_strings(s);
				if(s!=null)
				{
							if(s[0].equalsIgnoreCase(Node.tag_END		)){return stepUp();
					}else 	if(s[0].equalsIgnoreCase(Node.tag_SINGLE	)){return pushChild(Node.NodeSingle(s));
//					}else 	if(s[0].equalsIgnoreCase(Node.tag_EXEC		)){return pushChild(Node.NodeEXEC(s));
					}else 	if(s[0].equalsIgnoreCase(Node.tag_REFC		)){return pushChild(Node.NodeREFC(s));
					}else 	if(s[0].equalsIgnoreCase(Node.tag_FETCH		)){return pushContaneer(Node.NodeFetch(s));
					}else 	if(s[0].equalsIgnoreCase(Node.tag_PRN		)){return pushChild(Node.NodePrn(s));
					
					}else 	if(s[0].equalsIgnoreCase(Node.tag_ELSE		)){return stepSwitch(Node.ID_NCHILDS_ELSE);					
					}else 	if(s[0].equalsIgnoreCase(Node.tag_EMPTY		)){return stepSwitch(Node.ID_NCHILDS_EMPTY);
					}else 	if(s[0].equalsIgnoreCase(Node.tag_ZEBRA		)){return stepSwitch(Node.ID_NCHILDS_ZEBRA);
					}else 	if(s[0].equalsIgnoreCase(Node.tag_HEADER	)){return stepSwitch(Node.ID_NCHILDS_HEADER);
					
					}else 	if(s[0].equalsIgnoreCase(Node.tag_BLOCK		)){ Node nn=Node.NodeBlock(s);
																			Node  rr=stack.get(0);
																			if(rr!=null)rr.RootAddNamedBlock(nn);
																			return pushContaneer(nn);
					}else 	if(s[0].equalsIgnoreCase(Node.tag_PUTBLOCK	)){return pushChild(Node.NodePutBlock(s));
					}else 	if(s[0].equalsIgnoreCase(Node.tag_SSET		)){return pushChild(Node.NodeSset(s));
					
				 	
				 	 
					
					
					}else 													return pushChild(Node.NodeStatic(tag_Begin+t+tag_End)); 
						
				};
		};
		return false;
	};
	//=====================================================
	public Node ParceTemplate(String src) throws Exception
	{
		   start_parce();
		   String o;
		   boolean allOk=true;
		   while(((o = get(src))!=null)&&(allOk))
		   {
			   
			   switch (rettype)
			   {
				   case 0:allOk = pushChild(Node.NodeStatic(o));break;
				   case 1:allOk = pushNode(o);break;
				   case 2:allOk = false;
			   }
		   };
		   if(o!=null)   		   throw new Exception("Error parcing at "+cor_pos);
		   if (stack.size()!=1)	   throw new Exception("Error tags closing ");

		   Node n=stack.get(0);
		   start_parce();
		   return n;		
	}
}

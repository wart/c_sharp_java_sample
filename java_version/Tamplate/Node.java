package Tamplate;


import java.util.Vector;

public class Node
{
	public final static String tag_ROOT   = "ROOT"; 

	public final static String tag_STATIC = "STATIC";
	public final static String tag_PRN    = "PRN";
	public final static String tag_SINGLE = "SINGLE"; 
	public final static String tag_EXEC   = "EXEC"; 
	public final static String tag_REFC   = "REFC";

	public final static String tag_FETCH  = "FETCH"; 
 	public final static String tag_IF     = "IF";
 	public final static String tag_BLOCK  = "BLOCK";
 	public final static String tag_PUTBLOCK  = "PUTBLOCK";

 	public final static String tag_SSET    = "SSET";
 	
 	public final static String tag_ELSE   = "ELSE";
 	public final static String tag_EMPTY  = "EMPTY";
 	public final static String tag_ZEBRA  = "ZEBRA";
 	public final static String tag_HEADER  = "HEADER";
 	public final static String tag_END    = "END";
 	
 	//===================================================
	public final static int		ID_NODE_ROOT		=	0; 
	public final static int		ID_NODE_PRN			=	1;
	public final static int		ID_NODE_SINGLE		=	2; 
	public final static int		ID_NODE_FETCH		=	3; 
 	public final static int		ID_NODE_STATIC		=	4; 
 	public final static int		ID_NODE_IF			=	5;
 	public final static int		ID_NODE_BLOCK		=	6;
 	public final static int		ID_NODE_PUTBLOCK	=	7;
 	public final static int		ID_NODE_SSET		=	8;
 	public final static int		ID_NODE_EXEC		=	9;
 	public final static int		ID_NODE_REFC		=  10;
 	
 	
 	
 	public final static int		ID_NCHILDS_ELSE		=	1;	//	childs1
 	public final static int		ID_NCHILDS_ZEBRA    =	2;	//	childs1
 	public final static int		ID_NCHILDS_EMPTY	=	3;	//	childs2
 	public final static int		ID_NCHILDS_HEADER	=	4;	//	childs3

 	
 	private final static int  prn_ofs = 4;
	//--------------------------------------------------
	public int 				Class;
	public String 			name;
	public String 			Value;
	public int 				chmode;
	public Vector<Node> 	childs0;
	public Vector<Node> 	childs1;
	public Vector<Node> 	childs2;
	public Vector<Node> 	childs3;
	public Vector<String>	param;
	//--------------------------------------------------
	private Node()
	{
		childs0=null;
		childs1=null;
		childs2=null;
		chmode=0;
		param=null;
	};
	private Node(int cl)
	{
		Class=cl;
	}
	//--------------------------------------------------
	public String ClassName()
	{
		switch(Class)
		{
			case	ID_NODE_ROOT	:return tag_ROOT; 
			case	ID_NODE_PRN		:return tag_PRN;
			case	ID_NODE_SINGLE	:return tag_SINGLE; 
			case	ID_NODE_FETCH	:return tag_FETCH; 
			case	ID_NODE_STATIC	:return tag_STATIC; 
			case	ID_NODE_IF		:return tag_IF;
			case	ID_NODE_BLOCK	:return tag_BLOCK;
			case	ID_NODE_PUTBLOCK:return tag_PUTBLOCK;
			case	ID_NODE_SSET	:return tag_SSET;
			case	ID_NODE_EXEC	:return tag_EXEC;
			case	ID_NODE_REFC	:return tag_REFC;
			
		};
		return "Unknown";
	};
	//--------------------------------------------------
	public boolean allowBlockBuildInPlace()
	{
		if(Class==ID_NODE_BLOCK)
			if (Value!=null)
				return Value.equalsIgnoreCase("true");
		return false;
	};
	//--------------------------------------------------
	private boolean ParceParams(String prms)
	{
		if(prms!=null)
			if(!prms.equalsIgnoreCase(""))
			{
				String[] s=prms.split(",");
				int sz=s.length;
				if(sz>0)
				{
					param=new Vector<String>();
					for(int i=0;i<sz;i++)
					{
						param.add(s[i].trim());
					};
				};
			};
		return true;
	}
	//--------------------------------------------------
	public void AddNode(Node n)
	{
		switch(chmode)
		{
			case 0:if (childs0==null)childs0=new Vector<Node>();childs0.add(n);break;
			case 1:if (childs1==null)childs1=new Vector<Node>();childs1.add(n);break;
			case 2:if (childs2==null)childs2=new Vector<Node>();childs2.add(n);break;
			case 3:if (childs3==null)childs3=new Vector<Node>();childs3.add(n);break;
		};
	};
	//--------------------------------------------------
	public void RootAddNamedBlock(Node n)
	{
		if(Class==ID_NODE_ROOT)
		{
			if(childs2==null)childs2=new Vector<Node>();
				childs2.add(n);
		};
	}
	//--------------------------------------------------
	public Node RootGetNamedBlock(String nam)
	{
		Node n=null;
		if(Class==ID_NODE_ROOT)
			if(childs2!=null)
			{
				int sz=childs2.size();
				for (int i=0;i<sz;++i)
					if((n=childs2.get(i))!=null)
						if (n.name!=null)
							if (n.name.equalsIgnoreCase(nam))
								return n;
			}
		return null;	
	}
	//==================================================
	public boolean allowAndSwitchCh2(int mm)
	{
		switch(Class)
		{
			case	ID_NODE_FETCH	:
				
				switch(mm)
				{
					case	ID_NCHILDS_ZEBRA:	if(childs1==null){chmode=1;return true;};
						break;
					case	ID_NCHILDS_EMPTY:	if(childs2==null){chmode=2;return true;};
						break;
					case	ID_NCHILDS_HEADER:	if(childs3==null){chmode=3;return true;};
						break;
				};
				break; 
			case	ID_NODE_IF		:
				switch(mm)
				{				
					case	ID_NCHILDS_ELSE:	if(childs1==null){chmode=1;return true;};
					break;
				};
				break;
		};
		return false;
	};
	//==================================================
	static public Node NodeRoot()
	{
		return new Node(ID_NODE_ROOT);
	}
	//--------------------------------------------------
	static public Node NodeBlock(String[] s)
	{
		Node n=null;
		if(s.length>=2)
			if (s[1]!=null)
				if (s[1].length()>0)
				{
					n=new Node(ID_NODE_BLOCK);
					n.name=s[1];
					if(s.length>=3)
					{
						n.Value=s[2].toUpperCase().trim();
					};
				};
		return n;		
	}
	//--------------------------------------------------
	static public Node NodeSset(String[] s)
	{
		Node n=null;
		if(s.length>2)
			if (s[1]!=null)
				if (s[1].length()>0)
				{
					n=new Node(ID_NODE_SSET);
					n.name=s[1];
					n.Value=s[2];
				};
		return n;		
	}	
	//--------------------------------------------------
	static public Node NodePutBlock(String[] s)
	{
		Node n=null;
		if(s.length>1)
		{
			n=new Node(ID_NODE_PUTBLOCK);
			n.name=s[1];
		};
		return n;			
	}	
	//--------------------------------------------------	
	static public Node NodePrn(String[] s)
	{
		Node n=null;
		if(s.length>1)
		{
			n=new Node(ID_NODE_PRN);
			n.name=s[1];
		};
		return n;
	}	
	//--------------------------------------------------
	static public Node NodeSingle(String[] s)
	{
		Node n=null;
		if(s!=null)
		{
			if(s.length>2)
			{
				n=new Node(ID_NODE_SINGLE);
				n.name=s[1];
				n.Value=s[2];
				if (s.length>3)
					n=(n.ParceParams(s[3]))?n:null;
				if ((n.name.length()==0)||(n.Value.length()==0)) n=null;
			}
		};
		return n;
	}
	//--------------------------------------------------
	static public Node NodeEXEC(String[] s)
	{
		Node n=null;
		if(s!=null)
		{
			if(s.length>1)
			{
				n=new Node(ID_NODE_EXEC);
				n.Value=s[1];
				if (s.length>2)
					n=(n.ParceParams(s[2]))?n:null;
				if (n.Value.length()==0) n=null;
			}
		};
		return n;
	}	
	//--------------------------------------------------
	static public Node NodeREFC(String[] s)
	{
		Node n=null;
		if(s!=null)
		{
			if(s.length>1)
			{
				n=new Node(ID_NODE_REFC);
				n.Value=s[1];
				if (s.length>2)
					n=(n.ParceParams(s[2]))?n:null;
				if (n.Value.length()==0) n=null;
			}
		};
		return n;
	}		
	//--------------------------------------------------
	static public Node NodeFetch(String[] s)
	{
		Node n=null;
		if(s!=null)
		{
			if(s.length>=3)
			{
				n=new Node(ID_NODE_FETCH);
				n.name=s[1];
				n.Value=s[2];
				if (s.length>=4)
					n=(n.ParceParams(s[3]))?n:null;
				if (n!=null)
				if ((n.name.length()==0)||(n.Value.length()==0)) n=null;
			}
		};
		return n;
	}	
	//--------------------------------------------------
	static public Node NodeIf(String[] s)
	{
		
		
		
		return null;
	}
	//--------------------------------------------------
	static public Node NodeStatic(String s)
	{
		Node n=new Node(ID_NODE_STATIC);
		n.Value=s;
		return n;
	}	
	//==================================================
	private static void prn				(StringBuffer sbb,int ofs,String txt)
	{
		if(sbb==null)
		{
		for(int i=0;i<ofs;i++)
			System.out.print(" ");
		System.out.println(txt);
		}else
		{
			for(int i=0;i<ofs;i++)
				sbb.append(" ");
			sbb.append(txt+"\n");
		}
	}
	//--------------------------------------------------
	private void Debug_prn_params	(StringBuffer sbb,int ofs)
	{
		if(param!=null)
		{
			prn(sbb,ofs,"Params:");
			int sz=param.size();
			for(int i=0;i<sz;i++)
				prn(sbb,ofs+prn_ofs,param.get(i));
		}else prn(sbb,ofs,"Params: Empty(null)");
	};
	//--------------------------------------------------
	private void Debug_prn_childs	(StringBuffer sbb,int ofs,Vector<Node> ch,String cap,boolean exclstatic)
	{
		if(ch!=null)
		{
			int sz=ch.size();
			prn(sbb,ofs,cap+"  ("+sz+"):");
			
			Node n=null;
			for(int i=0;i<sz;i++)
				if((n=ch.get(i))!=null)
				{
					n.Debug_prn(sbb,ofs+prn_ofs,exclstatic);
				}else prn(sbb,ofs+prn_ofs,"null child found");
		}else prn(sbb,ofs,cap+": Empty(null)");
	};
	//--------------------------------------------------	
	private void Debug_prn			(StringBuffer sbb,int ofs,boolean exclstatic)
	{
		switch (Class)
		{
			case	ID_NODE_ROOT	:prn(sbb,ofs,"Node Class: "+ClassName());
									 Debug_prn_childs(sbb,ofs,childs0,"Childs",exclstatic);
									 Debug_prn_childs(sbb,ofs,childs2,"Named Blocks",exclstatic);
									 prn(sbb,0,"");
			break;
			case	ID_NODE_PRN		:prn(sbb,ofs,"Node Class: "+ClassName());
									 prn(sbb,ofs,"FIELD: '"+name+"'");
									 prn(sbb,0,"");
			break;
			case	ID_NODE_PUTBLOCK:prn(sbb,ofs,"Node Class: "+ClassName());
			 						 prn(sbb,ofs,"BLOCK: '"+name+"'");
			 						 prn(sbb,0,"");
			break;
			case	ID_NODE_BLOCK	:prn(sbb,ofs,"Node Class: "+ClassName());
			 						 prn(sbb,ofs,"Name: '"+name+"'");
			 						 prn(sbb,ofs,"Visibility: '"+Value+"'");
			 						 Debug_prn_childs(sbb,ofs,childs2,"Childs",exclstatic);
			 						 prn(sbb,0,"");
			break;						
			case	ID_NODE_SINGLE	:prn(sbb,ofs,"Node Class: "+ClassName());
			 						 prn(sbb,ofs,"NAME: '"+name+"'");
									 prn(sbb,ofs,"SQL: '"+Value+"'");
									 Debug_prn_params(sbb,ofs);
									 prn(sbb,0,"");
			break;
			case	ID_NODE_SSET	:prn(sbb,ofs,"Node Class: "+ClassName());
			 						 prn(sbb,ofs,"Param: '"+name+"'");
			 						 prn(sbb,ofs,"Value: '"+Value+"'");
			 						 prn(sbb,0,"");
			break;
			case	ID_NODE_FETCH	:prn(sbb,ofs,"Node Class: "+ClassName());
									 prn(sbb,ofs,"NAME: '"+name+"'");
									 prn(sbb,ofs,"SQL: '"+Value+"'");
									 Debug_prn_params(sbb,ofs);
									 Debug_prn_childs(sbb,ofs,childs3,"Header",exclstatic);
									 Debug_prn_childs(sbb,ofs,childs0,"Childs",exclstatic);
									 Debug_prn_childs(sbb,ofs,childs1,"Zebra",exclstatic);
									 Debug_prn_childs(sbb,ofs,childs2,"Empty",exclstatic);
									 prn(sbb,0,"");
			break;
			case 	ID_NODE_IF		:prn(sbb,ofs,"Node Class: "+ClassName());
									
									// TODO:
			
									 Debug_prn_childs(sbb,ofs,childs0,"True Childs",exclstatic);
									 Debug_prn_childs(sbb,ofs,childs1,"Else Childs",exclstatic);
									 prn(sbb,0,"");
			case	ID_NODE_STATIC	:if(!exclstatic)
									{
										prn(sbb,ofs,"Node Class: "+ClassName());
										prn(sbb,ofs,"text: '"+Value+"'");
										prn(sbb,0,"");
									}else prn(sbb,ofs,"...");
			break;
			case	ID_NODE_EXEC	:prn(sbb,ofs,"Node Class: "+ClassName());
			 						 prn(sbb,ofs,"SQL: '"+Value+"'");
			 						 Debug_prn_params(sbb,ofs);
			 						 prn(sbb,0,"");
            break;			
			case	ID_NODE_REFC	:prn(sbb,ofs,"Node Class: "+ClassName());
									 prn(sbb,ofs,"SQL: '"+Value+"'");
									 Debug_prn_params(sbb,ofs);
									 prn(sbb,0,"");
			break;			
			default:
				prn(sbb,ofs,"Wrong Node class");
		}
		
	}
	//--------------------------------------------------	
	public static void Debug_prn	(StringBuffer sbb,Node n,boolean exclstatic)
	{
		   if(n!=null)
		   {
			   n.Debug_prn(sbb,0,exclstatic);
		   }else
			   prn(sbb,0,"Node is null");
		   
	}
	//--------------------------------------------------	
}



	 import java.awt.Dimension;
import java.awt.Insets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.zefer.pd4ml.PD4ML;
import org.zefer.pd4ml.PD4Constants;

public class test2
{
	protected Dimension format = PD4Constants.A4;
	protected boolean landscapeValue = false;
	protected int topValue = 10;
	protected int leftValue = 10;
	protected int rightValue = 10;
	protected int bottomValue = 10;
	protected String unitsValue = "mm";
	protected String proxyHost = "";
	protected int proxyPort = 0;
	protected int userSpaceWidth = 780;
	private void runConverter(String urlstring, OutputStream output) throws IOException
	{
		if (urlstring.length() > 0)
		{
			if (!urlstring.startsWith("http://") && !urlstring.startsWith("file:"))
			{
				urlstring = "http://" + urlstring;
			};
			//java.io.FileOutputStream fos = new java.io.FileOutputStream(output);
			if ( proxyHost != null && proxyHost.length() != 0 && proxyPort != 0 )
			{
				System.getProperties().setProperty("proxySet", "true");
				System.getProperties().setProperty("proxyHost", proxyHost);
				System.getProperties().setProperty("proxyPort", "" + proxyPort);
			}
			PD4ML pd4ml = new PD4ML();
			pd4ml.enableDebugInfo();
			
			//pd4ml.generatePdfa(true);
			//pd4ml.enableImgSplit(true);
			//pd4ml.setPermissions(pd4ml.PD4ML_ALLOWED_RESOURCE_LOCATION,1,true);
			//pd4ml.isDemoMode()
			
			pd4ml.useTTF("c:/windows/fonts", true);
			pd4ml.useTTF("file://localhost/c:/windows/fonts", true);
			pd4ml.useTTF("file:///c:/windows/fonts", true);
			
			
			
			
			try
			{                                                               
				pd4ml.setPageSize( landscapeValue ? pd4ml.changePageOrientation( format ): format );
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			if ( unitsValue.equals("mm") )
			{
				pd4ml.setPageInsetsMM( new Insets(topValue, leftValue,bottomValue, rightValue) );
			}else
			{
				pd4ml.setPageInsets( new Insets(topValue, leftValue,bottomValue, rightValue) );
			}
			pd4ml.setHtmlWidth( userSpaceWidth );
			
			//pd4ml.render(arg0, arg1)
			
//			InputStreamReader isr = new InputStreamReader(new FileInputStream(new File("e:/reporter.htm").getAbsolutePath()), "windows-1251");
//			pd4ml.render( isr, output );
			pd4ml.render( urlstring, output );
		}
	}
	//-------------------------------------------------------------
	public static void main(String[] args) throws Exception
	{
		//http://127.0.0.1:50975/GWTB.html?gwt.codesvr=127.0.0.1:9997
		test2 t= new test2();
		FileOutputStream fos= new FileOutputStream( new File("e:/19.pdf"));
		
		//t.runConverter("http://127.0.0.1:51008/reports/schet2.htm",fos);
		t.runConverter("http://127.0.0.1:50994/gwtb/reporter?repname=schet2&RAW=yes&bid=201005&uid=72783",fos);
		
	}
	//-------------------------------------------------------------
}

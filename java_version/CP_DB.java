import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

import oracle.jdbc.OracleDriver;

public class CP_DB{
	  public static void Log(String str) {
		//    System.err.println(new Date() + " >> " + str);
		  }	
	
    public  static void       initPull(String Name, String connstr,String user,String pwd)
    {
    	Log("CP_DB::initPull start");
    	Log("CP_DB::initPull ARGS");
    	Log("Name: "+Name);
    	Log("connstr: "+connstr);
    	Log("user: "+user);
    	Log("pwd: "+pwd);
    	
       Init();
       CP_DB p= findPull(Name);
       if (p==null)
       {
           p= new CP_DB(Name,connstr,user,pwd);
           pulls.add(p);
           last_used_pull=p;
       }
       
   	Log("CP_DB::initPull done"+((p!=null)?" inited":"not inited"));
    }
//=================================================================================    
//=================================================================================
    public  static Connection getRAWlConnection(String url, String user, String password){
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException ex) {
        	System.err.println("CP_DB::getNoPullConnection");
            return null;
        }
    }
  //=================================================================================
    public  static void       freeRAWConnection(Connection connection){
        if (connection!=null)
        {
            try
            {
                connection.close();
            } catch (SQLException ex) {
            	Log("CP_DB::freeRAWConnection");
            }
        }
    }
//=================================================================================
    public  static Connection getConnection(String PullName)    {
    	Log("CP_DB::getConnection start");
        CP_DB p= findPull(PullName);
        if (p!=null){
        	Log("CP_DB::getConnection done");
            return p.getConnection();
        }
        Log("CP_DB::getConnection done");
        return null;
    }    
  //=================================================================================
    public  static void       freeConnection(String pname,Connection connection){
        CP_DB p=null;
        if ((p=findPull(pname))!=null)
        {
       		p.freeConnection(connection);
        }
    }
//=================================================================================
    public  static Connection getPullRAWConnection(String PullName){
        CP_DB p= findPull(PullName);
        if (p!=null)
            return p.getPullRAWConnection();
        return null;
    };
  //=================================================================================
    public  static Connection getPullRAWConnection(String PullName,String usr,String pwd){
        CP_DB p= findPull(PullName);
        if (p!=null)
            return p.getPullRAWConnection(usr,pwd);
        return null;
    };    
    public  static void       freePullRAWConnection(Connection connection){
       freeRAWConnection(connection);
   }
//=================================================================================    
    public  static Connection getNamedConnection        (String nameconn,String urlconn, String userconn, String pwdconn){
        
        Connection c=findNamedConn(nameconn);
        if (c==null)
        {
            c=getRAWlConnection(urlconn,userconn,pwdconn);
            NamedConnections.add(NamedConnection.GetInstance(nameconn,c));
        }
        return c;
    }
    public  static Connection getNamedConnectionFromPull(String nameconn,String pullname){
        Connection c=findNamedConn(nameconn);
        if (c==null)
        {
            c=getPullRAWConnection(pullname);
            if (c!=null){
            NamedConnections.add(NamedConnection.GetInstance(nameconn,c));                
            }
        }
        return c;
    }    
    public  static Connection getNamedConnectionFromPull(String nameconn,String pullname, String userconn, String pwdconn){
        Connection c=findNamedConn(nameconn);
        if (c==null)
        {
            c=getPullRAWConnection(pullname,userconn,pwdconn);
            if (c!=null){
            NamedConnections.add(NamedConnection.GetInstance(nameconn,c));                
            }
        }
        return c;
    }    
    //=================================================================================
    public  static void       freeNamedConnection(String name){
        int k=NamedConn_present(name);
        if (k>=0){
            NamedConnection f=NamedConnections.elementAt(k);
            if (f.release())
                NamedConnections.remove(k);
        }
    }
    //=================================================================================
    public  static void       freeNamedConnection(Connection conn){
        int k=NamedConn_present(conn);
        if (k>=0){
            NamedConnection f=(NamedConnection)NamedConnections.elementAt(k);
            if (f.release())
                NamedConnections.remove(k);
        }
    }    
//=================================================================================

    private static final  int PULL_MAX_CONNECTIONS = 16;
//=================================================================================
    private static            Vector<CP_DB>  	pulls            = null;    
    private static            Vector<NamedConnection>  NamedConnections = null;           
    private static            CP_DB           last_used_pull   = null;
    private static            NamedConnection last_used_conn   = null;
//=================================================================================
    private static int        pull_present(String nm){
    	Log("CP_DB::pull_present start: found");
        if (pulls!=null){
            for ( int j=0;j<pulls.size();j++){
            CP_DB pc=(CP_DB)(pulls.elementAt(j));
            if (pc!=null){if(pc.is(nm)){
            	Log("CP_DB::pull_present done: present");
            	return j;}}
            }
        }
        Log("CP_DB::pull_present done: not present");
        return -1;
    }
  //=================================================================================
    private static CP_DB      findPull(String pname)
    {
    	Log("CP_DB::findPull start");
    		if ((last_used_pull!=null)&&(last_used_pull.is(pname))) return last_used_pull;
    		int k=pull_present(pname);
    		if (k>=0)
    		{
    			synchronized(pulls)
    			{
    				CP_DB f=(CP_DB)pulls.elementAt(k);
    				last_used_pull=f;
    				Log("CP_DB::findPull done: found");
    				return f;
    			}
    		}
    		Log("CP_DB::findPull done: notfound");
    		return null;
    };
//=================================================================================
    private static void       Init(){
    	Log("CP_DB::Init start");
        if (pulls==null){
          pulls= new Vector<CP_DB>();
        if (NamedConnections==null){
            NamedConnections=new Vector<NamedConnection>();
        }
       try{
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            Class.forName("org.postgresql.Driver").newInstance();

           DriverManager.registerDriver(new OracleDriver());
    	   
            
          }catch(SQLException ex){
        	  Log("ConnectionPullMySQL::Init SQLException");
/*          }catch(ClassNotFoundException e){
        	  System.err.println("ConnectionPullMySQL::Init ClassNotFoundException");
          }catch(IllegalAccessException e){
        	  System.err.println("ConnectionPullMySQL::Init IllegalAccessException");
          }catch(InstantiationException e){
        	  System.err.println("ConnectionPullMySQL::Init InstantiationException");
          /**/
           }
         
        }
        Log("CP_DB::Init done");
    }
  //=================================================================================
    private static int        NamedConn_present(String nm)   {
        if (NamedConnections!=null){
            for ( int j=0;j<NamedConnections.size();j++){
            NamedConnection pc=NamedConnections.elementAt(j);
            if (pc!=null){if(pc.is(nm)){return j;}}
            }
        }
        return -1;
    }
  //=================================================================================
    private static int        NamedConn_present(Connection conn)   {
        if (NamedConnections!=null){
            for ( int j=0;j<NamedConnections.size();j++){
            NamedConnection pc=NamedConnections.elementAt(j);
            if (pc!=null){if(pc.is(conn)){return j;}}
            }
        }
        return -1;
    }    
  //=================================================================================
    private static Connection findNamedConn(String Connname){
      if ((last_used_conn!=null)&&(last_used_conn.is(Connname))) return last_used_conn.get();
      int k=NamedConn_present(Connname);
      if (k>=0){
          NamedConnection f=NamedConnections.elementAt(k);
          last_used_conn=f;
        return f.get();
      }
      return null;
    };
//=================================================================================
//=================================================================================
    private        String     pull_name="";
    private        Connection connections[];
    private        int        usedCounts[];
    private        String     thinConn;
    private        String     username;
    private        String     password;
//=================================================================================   
    private        CP_DB        (String name, String connstr,String user,String pwd){
        pull_name=name;
        thinConn=connstr;
        username=user;
        password=pwd;
        connections = new Connection [PULL_MAX_CONNECTIONS];
        usedCounts = new int[PULL_MAX_CONNECTIONS];
        for (int i = 0; i < usedCounts.length; i++)
            usedCounts[i] = 0;
    };
  //=================================================================================
    private        boolean    is(String nm)
    {
        return (pull_name!=null)?pull_name.equals(nm):false;
    }
  //=================================================================================
    private        int        getMinUsedConnection(){
      int forUse = 0;
      for (int i = 1; i < connections.length; i++)
            if (usedCounts[i] < usedCounts[forUse])
                forUse = i;
        return forUse;
    }
  //=================================================================================
    private        Connection getConnection(){
    	Log("private CP_DB::getConnection start");
      Connection connection=null;
      int forUse = getMinUsedConnection();
    		  if (connections[forUse] == null)
    			  connections[forUse] =getPullRAWConnection();
    		  connection = connections[forUse];
    		  usedCounts[forUse]++;
      Log("private CP_DB::getConnection done "+((connection!=null)?"getted":"null"));
      return connection;
    };
  //=================================================================================
    private        void       freeConnection(Connection connection){
    if (connection!=null)
    {
        for (int i = 0; i < connections.length; i++)
          if (connection.equals(connections[i])) {
            usedCounts[i]--;
            if (usedCounts[i] == 0) {
              freeRAWConnection(connections[i]);
              connections[i] = null;
            }
            return;
          }
      }
    };
//=================================================================================
    private        Connection getPullRAWConnection(){
    	Log("private CP_DB::getPullRAWConnection start");
       Connection c=null;
      try{
        c=DriverManager.getConnection(thinConn, username, password);
        c.setAutoCommit(false);                
      }catch(SQLException e){
        c=null;
        Log("private CP_oracle::getPullRAWConnection"+e.getMessage() );
      }
      Log("private CP_DB::getPullRAWConnection done");
      return c;
    }
//=================================================================================
    private        Connection getPullRAWConnection(String usr,String pwd){
       Connection c=null;
      try{
        c=DriverManager.getConnection(thinConn, usr, pwd);
        c.setAutoCommit(false);                
      }catch(SQLException e){
        c=null;
        Log("CP_oracle::getPullRAWConnection");
      }
      return c;
    }
//=================================================================================        
}



import java.sql.Connection;

 public class NamedConnection{
        private String pcname;
        private Connection pcconn;
        private Integer pcuseCount;
        
        public NamedConnection(String n,Connection c){
            pcname=n;
            pcconn=c;
            pcuseCount=1;
        }
        
        public boolean    is(String nm) {
          return pcname.equals(nm);
        }
        public boolean    is(Connection c) {
          return pcconn.equals(c);
        }
        
        public Connection get(){
        	synchronized(pcuseCount)
        	{
            pcuseCount++;
        	};
            return pcconn;
        }
        public boolean isused(){
            return pcuseCount>0;
        }
        
        public static NamedConnection GetInstance(String n,Connection c){
            return new NamedConnection(n,c);
        }
        public boolean release(){
        	synchronized(pcuseCount)
        	{
            pcuseCount--;
            if (pcuseCount<1){
                CP_DB.freeRAWConnection(pcconn);
                pcconn=null;
                pcname=null;
            }
            return pcuseCount<1;
        	}
        }
    }